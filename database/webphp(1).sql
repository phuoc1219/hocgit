-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th7 09, 2020 lúc 04:05 PM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `webphp`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(150) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `phone`, `address`, `password`) VALUES
(26, 'nguyễn văn thành', 'vanthanh@gamil.com', '0967888333', 'tổ 6 khu phố 4', '25d55ad283aa400af464c76d713c07ad'),
(27, 'nguyễn tấn phước', 'nguyentanphuoc@gmail.com', '0967990105', 'tổ 6 khu phố 4', '54e1ffbec1e8dfbfd2b852c1e4553e12'),
(29, 'nguyên thành M', 'thanhnguyenM@gmail.com', '4545454545', 'dfdfdfd', '25d55ad283aa400af464c76d713c07ad'),
(30, 'nguyễn tấn phước', 'nguyentanphuoc123@gmail.com', '0967990105', '128 hà huy giap Q.12 Phường Thạnh Lộc', '54e1ffbec1e8dfbfd2b852c1e4553e12');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `sort_order` tinyint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`id`, `name`, `parent_id`, `sort_order`) VALUES
(1, 'bút viết', 3, 3),
(2, 'sách', 0, 2),
(3, 'dụng cụ Học Tập', 0, 3),
(7, 'sách giáo khoa', 2, 3);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `phone` int(11) NOT NULL,
  `address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `map` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `contact`
--

INSERT INTO `contact` (`id`, `phone`, `address`, `email`, `map`) VALUES
(6, 2147483647, 'dfdfdfd', 'nguyenhadong3333@gmail.com', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3918.404390319125!2d106.6775625143612!3d10.856815592267028!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317528359904df3b%3A0x53c3188de1eae2ce!2zMTM4IEjDoCBIdXkgR2nDoXAsIFRo4bqhbmggTOG7mWMsIFF14bqtbiAxMiwgSOG7kyBDaMOtIE1pbmgsIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1589799922062!5m2!1svi!2s');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `gioithieu`
--

CREATE TABLE `gioithieu` (
  `id` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `gioithieu`
--

INSERT INTO `gioithieu` (`id`, `content`) VALUES
(1, 'Nguồn nhân lực\r\n\r\nĐể xây dựng Thương hiệu mạnh, một trong những định hướng quan trọng hàng đầu của FAHASA là chiến lược phát triển nguồn nhân lực - mấu chốt của mọi sự thành công.\r\n\r\nFAHASA có hơn 2.200 CB-CNV, trình độ chuyên môn giỏi, nhiệt tình, năng động, chuyên nghiệp. Lực lượng quản lý FAHASA có thâm niên công tác, giỏi nghiệp vụ nhiều kinh nghiệm, có khả năng quản lý tốt và điều hành đơn vị hoạt động hiệu quả.\r\n\r\nKết hợp tuyển dụng nguồn nhân lực đầu vào có chất lượng và kế hoạch bồi dưỡng kiến thức, rèn luyện bổ sung các kỹ năng và chuẩn bị đội ngũ kế thừa theo hướng chính qui thông qua các lớp học ngắn hạn, dài hạn; các lớp bồi dưỡng CB-CNV được tổ chức trong nước cũng như ở nước ngoài đều được lãnh đạo FAHASA đặc biệt quan tâm và tạo điều kiện triển khai thực hiện. Chính vì thế, trình độ chuyên môn của đội ngũ CB-CNV ngày càng được nâng cao, đáp ứng nhu cầu ngày càng tăng của công việc cũng như sự phát triển của xã hội đang trên đường hội nhập.\r\n\r\nVề hàng hóa\r\n\r\nCông ty FAHASA chuyên kinh doanh: sách quốc văn, ngoại văn, văn hóa phẩm, văn phòng phẩm, dụng cụ học tập, quà lưu niệm, đồ chơi dành cho trẻ em… Một số Nhà sách trực thuộc Công ty FAHASA còn kinh doanh các mặt hàng siêu thị như: hàng tiêu dùng, hàng gia dụng, hóa  mỹ phẩm…\r\n\r\nSách quốc văn với nhiều thể loại đa dạng như sách giáo khoa – tham khảo, giáo trình, sách học ngữ, từ điển, sách tham khảo thuộc nhiều chuyên ngành phong phú: văn học, tâm lý – giáo dục, khoa học kỹ thuật, khoa học kinh tế - xã hội, khoa học thường thức, sách phong thủy, nghệ thuật sống, danh ngôn, sách thiếu nhi, truyện tranh, truyện đọc, từ điển, công nghệ thông tin, khoa học – kỹ thuật, nấu ăn, làm đẹp...  của nhiều Nhà xuất bản, nhà cung cấp sách có uy tín như: NXB Trẻ, Giáo Dục, Kim Đồng, Văn hóa -Văn Nghệ, Tổng hợp TP.HCM, Chính Trị Quốc Gia; Công ty Đông A, Nhã Nam, Bách Việt, Alphabook, Thái Hà, Minh Lâm, Đinh Tị, Minh Long, TGM, Sáng Tạo Trí Việt, Khang Việt, Toàn Phúc…\r\n\r\nSách ngoại văn bao gồm: từ điển, giáo trình, tham khảo, truyện tranh thiếu nhi , sách học ngữ, từ vựng, ngữ pháp, luyện thi TOEFL, TOEIC, IELS…được nhập từ các NXB nước ngoài như: Cambridge, Mc Graw-Hill, Pearson Education, Oxford, Macmillan, Cengage Learning…\r\n\r\nVăn phòng phẩm, dụng cụ học tập, đồ chơi dành cho trẻ em, hàng lưu niệm… đa dạng, phong phú, mẫu mã đẹp, chất lượng tốt, được cung ứng bởi các công ty, nhà cung cấp uy tín như: Thiên Long, XNK Bình Tây, Hạnh Thuận, Ngô Quang, Việt Văn, Trương Vui, Hương Mi, Phương Nga, Việt Tinh Anh, Chăm sóc trẻ em Việt, Mẹ và em bé…\r\n\r\nCùng với việc phát hành độc quyền nhiều ấn bản các loại của các Nhà xuất bản là năng lực in ấn, sản xuất cung ứng nguồn hàng của Xí nghiệp in FAHASA, đã giúp Công ty luôn chủ động được nguồn hàng, nhất là các mặt hàng độc quyền như: lịch bloc, tập học sinh, sổ tay cao cấp, agenda, văn phòng phẩm, dụng cụ học tập…\r\n\r\nHệ thống Nhà sách chuyên nghiệp\r\n\r\nMạng lưới phát hành của Công ty FAHASA rộng khắp trên toàn quốc, gồm 5 Trung tâm sách, 1 Xí nghiệp in và với gần 60 Nhà sách trải dọc khắp các tỉnh thành từ TP.HCM đến Thủ Đô Hà Nội, miền Trung, Tây Nguyên, miền Đông và Tây Nam Bộ như: Hà Nội, Vĩnh Phúc, Hải Phòng, Thanh Hóa, Hà Tĩnh, Huế, Đà Nẵng, Quảng Nam, Quảng Ngãi, Quy Nhơn, Nha Trang, Gia Lai, Đăklăk, Bảo Lộc - Lâm Đồng, Ninh Thuận, Bình Thuận, Bình Phước, Bình Dương, Đồng Nai, Vũng Tàu, Long An, Tiền Giang, Bến Tre, Vĩnh Long, Cần Thơ, Hậu Giang, An Giang, Kiên Giang, Sóc Trăng, Cà Mau.\r\n\r\nNăm 2004 Công ty đã được Cục Sở hữu Trí tuệ Việt Nam công nhận sở hữu độc quyền tên thương hiệu FAHASA.\r\n\r\nNăm 2005, Công ty FAHASA được Trung tâm sách kỷ lục Việt Nam công nhận là đơn vị có hệ thống Nhà sách nhiều nhất Việt Nam; được Tạp chí The Guide công nhận Nhà sách Xuân Thu - đơn vị trực thuộc Công ty FAHASA là Nhà sách Ngoại văn đẹp nhất, lớn nhất, quy mô nhất, nhiều sách nhất ở Thành phố Hồ Chí Minh và cả nước.\r\n\r\nNăm 2012 FAHASA là Doanh nghiệp nằm trong Top 10 nhà bán lẻ hàng đầu Việt Nam. Đặc biệt năm 2006, 2009, 2012 FAHASA đạt danh hiệu Top 500 Nhà bán lẻ hàng đầu Châu Á Thái Bình Dương, giải thưởng được tạp chí Retail Asia (Singapore) bình chọn.\r\n\r\nKinh nghiệm hoạt động\r\n\r\nLà đơn vị có gần 40 năm kinh doanh và phục vụ xã hội, nên FAHASA đã tích lũy được nhiều kinh nghiệm trong việc nghiên cứu thị trường, phân tích tài chính, định hướng phát triển, hoạch định chiến lược kinh doanh và khả năng tiếp thị giỏi… Đồng thời FAHASA còn có nhiều kinh nghiệm trong việc tổ chức các cuộc Hội thảo, Triển lãm và giới thiệu sách quốc văn, ngoại văn với qui mô lớn, ấn tượng.\r\n\r\nFAHASA luôn là đơn vị đi tiên phong trong nhiều hoạt động xã hội, điển hình là việc tham gia tổ chức các Hội sách ở TP.HCM như: Hội sách Thành Phố Hồ Chí Minh, Hội sách Thiếu nhi ngoại thành, Hội sách Mùa khai trường, Hội sách Học đường, Hội sách Trường Quốc tế… Nổi bật nhất là Hội sách Thành Phố Hồ Chí Minh, được định kỳ tổ chức 2 năm một lần. Đây là Hội sách có qui mô lớn, tầm ảnh hưởng rộng, đã để lại ý nghĩa kinh tế, văn hóa và xã hội sâu sắc. Hội sách không chỉ là nơi hội tụ văn hóa lý tưởng đối với người dân TP.HCM mà còn là một thông điệp văn hóa tôn vinh cho các hoạt động Xuất bản – Phát hành sách cả nước, nâng tầm cho việc giao lưu, trao đổi văn hóa với bạn bè thế giới, đồng thời góp phần đem sách - tri thức đến gần hơn với mọi tầng lớp nhân dân, phục vụ tốt hơn nhu cầu văn hóa tinh thần của xã hội\r\n\r\nXí nghiệp in FAHASA\r\n\r\nGồm Phân xưởng in và Phân xưởng thành phẩm: với nhiều máy in hiện đại của Đức và Nhật: Heidelberg, Komori, Akiyama, Lithrone, Mitsubishi… cùng nhiều máy móc, thiết bị khác như: máy cắt, máy vô bìa sách, máy bế hộp…  Đội ngũ công nhân tay nghề cao, đã cho ra những sản phẩm có chất lượng tốt, nhờ đó, FAHASA luôn chủ động được nguồn hàng, sản xuất theo đúng nhu cầu - thị hiếu của khách hàng và hình thành được quy trình in & phát hành; phương thức này không chỉ cho ra những sản phẩm đảm bảo chất lượng mà còn giúp cho việc giảm giá thành, tăng hiệu quả cạnh tranh và kinh doanh cao hơn.\r\n\r\nNhững sản phẩm chủ yếu do Xí nghiệp In FAHASA sản xuất như: tập học sinh, sổ tay cao cấp, lịch, bao bì, sổ notebook, agenda, catalogue, brochure quảng cáo, văn phòng phẩm…\r\n\r\nFAHASA NHÀ PHÂN PHỐI SÁCH NGOẠI VĂN CHUYÊN NGHIỆP\r\n\r\nDù là những bạn đọc nhỏ tuổi hay những bậc cao niên, dù là bạn đọc ở TP.HCM hay ở các tỉnh thành khác trên cả nước thì tên FAHASA đã trở nên thân quen và tin cậy với họ trong những năm qua. Có thể nói, hệ thống gần 60 Nhà sách của FAHASA là những điểm sinh hoạt văn hóa thân quen dành cho mọi đối tượng bạn đọc. Trong số đó, nhà sách Xuân Thu ở địa chỉ cũ số 185, Đồng Khởi, Quận 1, TP.HCM, nay chuyển về 391-391A Trần Hưng Đạo, Quận 1, TP.HCM tọa lạc tại một địa điểm khá lý tưởng nằm ở trung tâm Thành phố, từ lâu đã là địa chỉ quen thuộc của đông đảo bạn đọc trong và ngoài nước và nơi đây được xem là địa điểm phát hành sách ngoại văn được xếp vào loại bậc nhất ở TP.HCM và của cả nước.\r\n\r\nCùng với xu thế hội nhập quốc tế đang ngày càng mở rộng, nhu cầu tìm hiểu và giao lưu văn hoá giữa các nước đang ngày càng phát triển, Công ty FAHASA ngày càng xứng đáng với tầm vóc là nhà phát hành đáng tin cậy của hơn 200 NXB trên Thế giới như Oxford, Cambridge, Pearson, McGraw-Hill, MacMillan, Cengage Learning, Reed Elsevier, Taylor & Francis, Heinemann, Hachette édition, Larousse, Clé International, Bắc Kinh, Thượng Hải, Hồng Kông… Thế mạnh của Công ty FAHASA trong lĩnh vực phân phối sách ngoại văn bao gồm cả hai mảng chính: sách học ngữ English language teaching (ELT) và mảng sách chuyên ngành (Academic).\r\n\r\nVề lĩnh vực sách ELT, hiện nay FAHASA đã và đang là nhà phân phối tất cả các loại sách học ngữ, từ điển, các giáo trình tiếng Anh với đủ mọi cấp độ cho các Trung tâm Anh ngữ, các trường Đại học ở TP.HCM nói riêng và cả nước nói chung thông qua các loại sách từ những NXB danh tiếng trên thế giới như Oxford, Cambridge, Pearson Education, Cengage Learning, McGraw-Hill… Đặc biệt, ở lĩnh vực này FAHASA là nhà phát hành độc quyền các ấn phẩm của NXB Oxford tại thị trường Việt Nam bộ sách Let’s Go; hợp tác với NXB Cambridge in ấn và phát hành tại Việt Nam một số bộ giáo trình Anh ngữ New Interchange, Connect, American Primary Colors; Vocabulary in use; Grammar in use… nhằm giảm bớt giá thành so với giá sách nhập khẩu, phục vụ nhu cầu tìm hiểu nâng cao vốn tiếng Anh của đông đảo độc giả.\r\n\r\nVề lĩnh vực sách chuyên ngành (Academic), FAHASA vẫn được xem là nhà phân phối lớn nhất các loại sách chuyên ngành phục vụ nhu cầu học tập, nghiên cứu cho các bạn sinh viên, các giáo viên, giáo sư, những người làm công tác nghiên cứu và hầu hết mọi đối tượng bạn đọc. FAHASA luôn năng động và nhạy bén trong việc nắm bắt nhu cầu của khách hàng, khai thác tối đa và phục vụ kịp thời nhu cầu của bạn đọc gần xa. Hiện nay, FAHASA đang là nhà phát hành cho các tập đoàn xuất bản lớn của Anh, Mỹ như NXB McGraw-Hill, Pearson Education, Cengage Learning, John Wiley…. Đến với cửa hàng sách ngoại văn của FAHASA bạn đọc sẽ tìm thấy hàng loạt các loại sách chuyên ngành bao gồm các thể loại đa dạng thuộc các lĩnh vực Kinh tế, Tin học, Y học, Kiến trúc, Hội họa, Khoa học kỹ thuật và các loại sách tham khảo khác.\r\n\r\nVới phương châm phục vụ quý khách ngày càng tốt hơn, Công ty FAHASA sẽ nỗ lực và phấn đấu hơn nữa để mang đến cho bạn đọc nhiều sách hay, sách tốt nên không chỉ ở Nhà sách Xuân Thu, Tân Định, Sài Gòn là nơi phát hành sách đầy đủ sách ngoại văn mà trong một tương lai không xa, bạn đọc có thể tìm mua bất kỳ các tựa sách nước ngoài nào ở hầu hết các cửa hàng trực thuộc FAHASA.\r\n\r\nTẦM NHÌN VÀ PHƯƠNG HƯỚNG PHÁT TRIỂN CỦA THƯƠNG HIỆU FAHASA TRONG TƯƠNG LAI\r\n\r\nTừ 2005 đến nay, FAHASA đã nhất quán và thực hiện thành công chiến lược xuyên suốt là xây dựng, phát triển Hệ thống Nhà sách chuyên nghiệp trên toàn quốc.Tính đến nay, sau hơn 6 năm thực hiện chiến lược, bên cạnh hệ thống gần 20 Nhà sách được hình thành từ năm 1976 và được phân bổ rộng khắp trên phạm vi TP.HCM, FAHASA đã cơ bản hoàn thiện giai đoạn 1 trong kế hoạch phát triển mạng lưới ở khắp các tỉnh thành trên toàn quốc với thành tựu: gần 80% các tỉnh thành miền Nam và miền Trung đều có mặt ít nhất một Nhà sách FAHASA. Một số tỉnh thành lớn đã có mặt Nhà sách thứ 2, thứ 3 của FAHASA như: Bình Dương, Đồng Nai, Cần Thơ, Đà Nẵng, Hà Nội…\r\n\r\nTiếp tục định hướng hoạt động chuyên ngành và thực hiện chiến lược phát triển mạng lưới, từ năm 2013 – 2020 FAHASA sẽ phát triển mạnh hệ thống Nhà sách tại các tỉnh thành phía Bắc. Hiện nay, Nhà sách FAHASA đã có mặt tại Hà Nội, Hà Tĩnh, Vĩnh Phúc, Hải Phòng, Thanh Hóa.\r\n\r\nDự kiến 2020, Hệ thống Nhà sách FAHASA sẽ có khoảng 100 Nhà sách trên toàn quốc. Tiếp tục giữ vững vị thế là hệ thống Nhà sách hàng đầu Việt Nam và nằm trong Top 10 nhà bán lẻ hàng đầu Việt Nam (tính cho tất cả các ngành hàng).\r\n\r\nDự án xây dựng Trung tâm sách tại TP.HCM với diện tích 5.000m² đến 10.000m², gồm đầy đủ các loại hình hoạt động về sách, phấn đấu xây dựng phong cách kinh doanh hiện đại, ngang tầm với các nước trong khu vực. FAHASA sẽ là kênh tiêu thụ chính của các Nhà xuất bản, các Công ty Truyền thông Văn hóa và là đối tác tin cậy của các Nhà cung cấp trong và ngoài nước. Đồng thời FAHASA giữ vũng vị trí Nhà nhập khẩu và kinh doanh sách ngoại văn hàng đầu Việt Nam.\r\n\r\nSỨ MỆNH CỦA FAHASA: “MANG TRI THỨC, VĂN HÓA ĐỌC ĐẾN VỚI MỌI NHÀ”!\r\n\r\nFAHASA là thương hiệu hàng đầu trong ngành Phát hành sách Việt Nam, ngay từ thời bao cấp cho đến thời kỳ kinh tế thị trường, đổi mới, hội nhập quốc tế, Công ty FAHASA luôn khẳng định vị thế đầu ngành và được đánh giá cao trong quá trình xây dựng đời sống văn hóa, trước hết là văn hóa đọc của nước nhà. Là doanh nghiệp kinh doanh trên lĩnh vực văn hóa, Công ty FAHASA có vai trò và vị thế trong việc giữ vững định hướng tư tưởng văn hóa của Nhà nước, góp phần tích cực vào việc đáp ứng nhu cầu đọc sách của Nhân dân Thành phố Hồ Chí Minh và cả nước; thể hiện toàn diện các chức năng kinh tế - văn hóa - xã hội. Thông qua các chủ trương và hoạt động như: duy trì một số Nhà sách ở các tỉnh có nền kinh tế chưa phát triển, công trình Xe sách Lưu động FAHASA phục vụ bạn đọc ngoại thành tại các huyện vùng sâu, vùng xa, định kỳ tổ chức các Hội sách với nhiều quy mô lớn nhỏ khác nhau… chứng minh rằng FAHASA không chỉ quan tâm đến việc kinh doanh mà còn mang đến mọi người nguồn tri thức quý báu, góp phần không ngừng nâng cao dân trí cho người dân ở mọi miền đất nước, thể hiện sự hài hòa giữa các mục tiêu kinh doanh và hoạt động phục vụ xã hội của FAHASA.\r\n\r\nHiện nay, Công ty FAHASA đã và đang ngày càng nỗ lực hơn trong hoạt động sản xuất kinh doanh, tiếp tục góp phần vào sự nghiệp phát triển “văn hóa đọc”, làm cho những giá trị vĩnh hằng của sách ngày càng thấm sâu vào đời sống văn hóa tinh thần của xã hội, nhằm góp phần tích cực, đáp ứng yêu cầu nâng cao dân trí, bồi dưỡng nhân tài và nguồn nhân lực cho sự nghiệp công nghiệp hóa, hiện đại hóa đất nước, trong bối cảnh Thành phố Hồ Chí Minh và xã hội Việt Nam đang ngày càng hội nhập sâu rộng vào nền văn hóa, kinh tế tri thức của thế giới.\r\n\r\nBẢNG VÀNG THÀNH TÍCH FAHASA ĐÃ ĐẠT ĐƯỢC TỪ NĂM 2008 - 2012 TRONG HOẠT ĐỘNG SẢN XUẤT KINH DOANH VÀ CÔNG TÁC XÃ HỘI\r\n\r\n     Cờ Thi đua của Chính phủ – Thủ tướng Chính phủ – ngày 28/4/2008.\r\n    Cờ thi đua Đơn vị xuất sắc năm 2008 của UBND TP.HCM.\r\n    Đạt giải thưởng « Sao vàng Phương Nam » và giải thưởng “Top 100 Sao vàng Đất Việt” của Ủy ban Trung ương Hội các nhà  doanh nghiệp Trẻ Việt Nam năm 2008.\r\n    Bằng khen của Bộ trưởng Bộ Thông tin & truyền thông hoàn thành tốt nhiệm vụ phát hành xuất bản phẩm năm 2008.\r\n    Top 500 nhà bán lẻ hàng đầu Châu Á – Thái Bình Dương do Tạp chí Bán lẻ Châu Á (Singapore) bình chọn năm 2009.\r\n    Top 10 Doanh nghiệp bán lẻ hàng đầu Việt Nam do tạp chí Retail Asia 2008 bình chọn.\r\n    Top Trade Services 2008 do Bộ Công Thương trao tặng.\r\n    Là doanh nghiệp phát hành sách duy nhất trong 10 doanh nghiệp bán lẻ hàng đầu Việt Nam được bình chọn nằm trong Top 500 nhà bán lẻ hàng đầu khu vực Châu Á - Thái Bình Dương do tạp chí Retail Asia năm 2009.\r\n    Top 500 doanh nghiệp lớn nhất Việt Nam VNR500 do Vietnamnet xếp hạng.\r\n    Giải thưởng Sao vàng đất Việt 2009  - Top 200 thương hiệu Việt Nam do TW Đoàn TNCS TP.HCM & Hội các Nhà Doanh Nghiệp Trẻ trao tặng.\r\n    Tổng Giám Đốc Phạm Minh Thuận được tặng Danh hiệu Doanh nhân Sài Gòn tiêu biểu năm 2009.\r\n    Cờ thi đua Đơn vị xuất sắc năm 2009 của UBND TP.HCM.\r\n    Cờ thi đua Đơn vị xuất sắc dẫn đầu phong trào thi đua năm 2009 của Bộ Thông tin – Truyền thông.\r\n    Thương hiệu Việt yêu thích nhất 2010 (do bạn đọc báo Sài Gòn Giải Phóng bình chọn).\r\n    Giải thưởng Sao vàng đất Việt 2010 – Top 200 thương hiệu Việt Nam do TW Đoàn TNCS TP.HCM & Hội các Nhà Doanh Nghiệp Trẻ trao tặng.\r\n    Cờ thi đua Đơn vị Xuất Sắc Năm 2010 của Bộ Thông Tin Truyền Thông trao tặng.\r\n    Cờ thi đua Đơn vị Xuất Sắc Năm 2010 của Uy Ban Nhân Dân TP.HCM trao tặng.\r\n    Top 100 Doanh Nghiệp Thương mại – Dịch Vụ Tiêu Biểu 2010 – VietNam Top Trade Service Award 2010.\r\n    Giải thưởng sao vàng đất Việt 4 năm liền từ 2008 đến 2011.\r\n    Giải thưởng Thương hiệu Việt được yêu thích nhất 2010, 2011.\r\n    Xe sách lưu động: Bằng khen của Ban Tuyên Giáo Trung ương năm 2011.\r\n    Top 50 Nhãn hiệu nổi tiếng, Top 500 doanh nghiệp lớn nhất Việt nam 2011.\r\n    Doanh nhân Sài Gòn tiêu biểu 2011.\r\n    Danh hiệu Hàng Việt Nam chất lượng cao 12 liền từ 2000 đến 2012.\r\n    Năm 2012, FAHASA được Thủ tướng chính phủ tặng bằng khen vì đã có thành tích trong công tác tổ chức Hội sách TP.Hồ Chí Minh liên tục nhiều năm qua.\r\n    Top 500 nhà bán lẻ hàng đầu Châu Á – Thái Bình Dương, Top 10 Nhà bán lẻ hàng đầu Việt Nam do Tạp chí Bán lẻ Châu Á (Singapore) bình chọn.\r\n\r\nBộ máy tổ chức');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `logo`
--

CREATE TABLE `logo` (
  `id` int(11) NOT NULL,
  `image_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `logo`
--

INSERT INTO `logo` (`id`, `image_link`) VALUES
(13, 'logo31.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `summary` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created` date NOT NULL,
  `image_link` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `news`
--

INSERT INTO `news` (`id`, `title`, `summary`, `content`, `created`, `image_link`) VALUES
(1, 'Review sách Tận cùng là cái chết ', 'Tận cùng là cái chết, một tác phẩm mang màu sắc độc đáo trong bộ sưu tập các kiệt tác của nữ hoàng trinh thám Agatha Christie. Tác phẩm đã xới tung từng góc khuất trong mỗi “cá thể người”, đặt ra câu hỏi: chân thật, lương thiện hay ích kỷ, đáng sợ mới là bản chất thật của con người.', 'Bối cảnh mới lạ về một Ai Cập cổ xưa, huyền bí\r\n<br>\r\n\r\nĐiểm hấp dẫn đầu tiên của truyện là bối cảnh mới lạ không theo môtip thường thấy trong những tiểu thuyết khác của Agatha Christie thường miêu tả cuộc sống ở Châu Âu gắn liền với ngài thám tử thiên tài có bộ râu độc đáo Poirot. “Tận cùng là cái chết” đưa độc giả đến với Ai Cập những năm 2000 trước công nguyên, bên cạnh con sông Nile hùng vĩ, những cánh đồng rượu vang, lúa mỳ trù phú dọc hai bên bờ sông, tìm hiểu sinh hoạt thường nhật của đại gia đình quan tư tế Imhotep hùng mạnh, chủ nhân của những thuyền buôn đầy ắp hàng hóa xuôi ngược khắp lưu vực sông Nile.\r\n<br>\r\nTruyện viết xuyên suốt, liền lạc như mạch chảy của con sông Nile qua các mùa trong năm với cốt truyện tương ứng với ba mùa chính tại Ai Cập cổ đại: mùa lũ, mùa đông (mùa trồng trọt) và mùa hạ (mùa thu hoạch). Cái tài tình trong bút pháp của Agatha là sự hợp lý hóa không gian và thời gian, bất kể bối cảnh câu chuyện tại miền quê châu Âu bình dị, trong căn nhà nghỉ trên đỉnh núi tuyết hoang vu cho đến Ai Cập cổ đại đều được miêu tả chân thực, sống động, tạo nên phông nền hoàn hảo cho tấn thảm kịch sắp diễn ra.\r\n<br>\r\nTuy không phải tiểu thuyết nổi tiếng nhất của nữ văn sĩ nhưng “Tận cùng là cái chết” vẽ nên một câu chuyện mang đậm màu sắc tâm linh, huyền bí diễn ra bên cạnh dòng sông thiêng hùng vĩ sẽ thỏa mãn độc giả yêu thích thể loại trinh thám, phiêu lưu,thích tìm hiểu về mảnh đất của Pharaoh và các vị thần.\r\nMọi thứ đều hoàn hảo cho đến khi nhân tố mới xuất hiện \r\n<br>\r\nNofret là cô gái trẻ xinh đẹp và có thừa thông minh, cô được vô số chàng trai tôn thờ, nhưng bất hạnh thay, người cô yêu lại không đáp lại. Bị từ chối trong tình yêu khiến Nofret trở thành một kẻ ôm lòng thù hận cuộc sống, để trả thù cuộc đời đã đối xử bất công với mình, cô chấp nhận làm vợ lẽ của ông già Imhotep, lấy việc chia rẽ gia đình đang được bao bọc trong nhung lụa, trên đỉnh tột cùng của sự giàu sang làm trò tiêu khiển.\r\n<br>\r\nĐược miêu tả là một nhân vật phản diện đặc trưng với vẻ đẹp của một vị nữ thần nhưng tính tình cay độc khó lường, Nofret, cô vợ lẽ của quan tư tế Imhotep là khởi nguồn tấn bi kịch của cả gia đình ông. Các thành viên trong gia đình Imhotep xem Nofret như con rắn độc tàn phá thành quả mà họ vất vả gây dựng được, nhưng thực tế chính người ngoài cuộc như Nofret và bà già mù Esa, mẹ của Imhotep lại là người nhìn thấu được con sóng ngầm bên dưới đế chế Imhotep. Lớp vỏ giàu có, phân cấp chặt chẽ tưởng như sợi dây liên kết không thể tách rời giữa các thành viên trong đại gia đình Imhotep chỉ là sự ngộ nhận. Như chính bà lão Esa lo sợ, cái ung nhọt không từ bên ngoài đến mà nảy sinh từ chính bên trong, nơi không ai có thể ngờ đến.\r\n<br>\r\nNếu khi còn sống Nofret khơi gợi nỗi đố kỵ, ghen ghét thì khi chết, cô trở thành lời nguyền chết chóc gieo rắc nỗi kinh hoàng đeo bám các thành viên nhà Imhotep. Tất cả chuỗi bi kịch đều được đổ lên đầu Nofret nhưng họ quên mất một điều lời nguyền của kẻ đã chết không đáng sợ bằng tâm địa của người sống. \r\nTận cùng của sự ích kỷ là tội ác không có điểm dừng\r\n<br>\r\nĐặc sản của tác giả Agatha Christie là lối dẫn dắt truyện tài tình với nhiều chi tiết hấp dẫn và cái kết bất ngờ theo môtip kẻ ít bị tình nghi nhất rất có thể là thủ phạm. Nguồn gốc tội ác trong những tiểu thuyết của nữ văn sĩ thường xoay quanh 2 nguyên nhân chính: tiền bạc và tình yêu,“Tận cùng là cái chết” cũng không ngoại lệ. Khối tài sản kếch xù với các vườn nho, ruộng lúa mỳ, đội tàu buôn và những mối làm ăn ngày càng phát triển của Imhotep là miếng mồi nhử hấp dẫn đủ sức khơi gợi những bản tính xấu xa nhất trong mỗi con người. Không những thế, với tính cách độc đoán, gia trưởng của mình, Imhotep đã vô tình tạo ra mảnh đất màu mỡ nuôi dưỡng tội ác.\r\n<br>\r\nTrong đại gia đình quan tư tế tất cả mọi thành viên đều mang bí mật riêng mà ngay cả vợ, chồng thân thuộc cũng không thể khám phá ra. Mỗi người như một diễn viên đeo chiếc mặt nạ người khác mong muốn để che giấu bản chất của mình, để có thể cùng nhau chung sống dưới một mái nhà. Người ích kỷ nhất chính là người chủ gia đình Imhotep, một kẻ bá vương, độc tài, luôn coi những đứa con của mình như nô lệ để ban phát ân huệ, ông ta luôn nghĩ “Chính vì chúng mà ta làm việc không nghỉ. Đôi khi ta tự hỏi chúng có nhận thức được chúng mang ơn ta như thế nào không?” Sự độc tài đã khiến những đứa con của ông ta đề phòng, ngấm ngầm đấu đá lẫn nhau để giành giật lợi ích cho bản thân, với hy vọng thoát khỏi sự điều khiển, kìm kẹp của cha mình.\r\n<br>\r\nImhotep có thể là con người quyền lực, mưu trí trong các mối làm ăn, xây dựng được đế chế buôn bán phát đạt dọc sông Nile nhưng ông lại là kẻ thất bại trong việc điều hành chính gia đình của mình. Vẻ tôn sùng, kính sợ của những người con, người hầu trong gia đình chỉ để che đậy mầm mống nổi loạn, bất mãn sâu trong họ, như chính Renisenb người con gái hiền dịu được ông yêu quý nhất cũng đã có lúc nảy ra suy nghĩ: “Có phải ông đã còm cõi lại? Hay trí nhớ của nàng là sai lầm? Nàng luôn nghĩ người cha là một người khá oai vệ, độc tài, khá huyên náo, khuyến khích người khác làm điều này, điều nọ và đôi khi ông làm nàng phải cười ngầm. Nhưng thế nào đi nữa, ông là một nhân vật quan trọng. Thế nhưng giờ đây trước mắt nàng là một ông già nhỏ bé, rắn chắc, đầy vẻ trịnh trọng, tuy nhiên, không thể gây một ấn tượng gì cho người khác. Có gì sai trong nàng đây? Sao lại có tư tuỏng bất trung như thế trong đầu nàng”.\r\n<br>\r\nKhi Imhotep ngày càng già yếu, những đứa con trai mà ông coi thường trước kia sẽ trỗi dậy như một điều tất yếu tre già măng mọc. Nhưng có lẽ sự kiêu ngạo, ích kỷ đã che mờ sự khôn ngoan sáng suốt của một con người một thời lẫy lừng, xây dựng nên cả một đế chế kinh doanh trải dài từ thượng nguồn đến hạ lưu sông Nile. Người Ai Cập có câu ngạn ngữ “giống như gió lốc xé nát cây và hủy hoại gương mặt của thiên nhiên trong cơn thịnh nộ, hay giống như động đất lật đổ cả thành phố khi rung chuyển, cơn thịnh nộ của một người sẽ gây ra sự tàn phá quanh anh ta”,  đúng lúc Imhotep dẫn người vợ kế trẻ đẹp về nhà cũng là lúc ông khơi lên cơn thịnh nộ âm ỉ bấy lâu, tự tay hủy hoại ngôi nhà pha lê nhìn đẹp đẽ nhưng có thể vỡ tan bất cứ lúc nào.\r\n<br>\r\n“Tận cùng là cái chết” không có sự xuất hiện của vị thám tử người Bỉ lừng danh Hercule Poirot hay bà cô Marple tinh quái, nhưng bù lại bối cảnh mới lạ về vùng đất Ai Cập cổ huyền bí khiến cho tác phẩm có nét độc đáo cuốn hút riêng. Dù với bối cảnh, cốt truyện như thế nào đi nữa thì Agatha vẫn luôn biết cách khiến độc giả phải bùng nổ khi đọc đến trang cuối cùng và “Tận cùng là cái chết” cũng không phải ngoại lệ.', '2020-06-16', '222.jpg'),
(3, 'review sách Thiên thần và ác quỷ ', 'Hấp dẫn, lôi cuốn và đầy tính nhân văn chính là những từ ngữ để nói về “Thiên thần và ác quỷ” – quyển tiểu thuyết thứ ba của nhà văn Dan Brown. Hơn cả một tác phẩm trinh thám đơn thuần, cuốn tiểu thuyết đã xuất sắc lồng ghép và đưa ra thông điệp sâu sắc về sự hòa hợp của tôn giáo và khoa học – hai vấn đề mà từ trước đến nay vẫn luôn được xem là đối lập nhau và không thể đi chung đường.', 'Hấp dẫn, lôi cuốn và đầy tính nhân văn chính là những từ ngữ để nói về “Thiên thần và ác quỷ” – quyển tiểu thuyết thứ ba của nhà văn Dan Brown. Hơn cả một tác phẩm trinh thám đơn thuần, cuốn tiểu thuyết đã xuất sắc lồng ghép và đưa ra thông điệp sâu sắc về sự hòa hợp của tôn giáo và khoa học – hai vấn đề mà từ trước đến nay vẫn luôn được xem là đối lập nhau và không thể đi chung đường.\r\n\r\nXuất bản năm 2000 và nhanh chóng lọt top sách bán chạy nhất của New York Times, “Thiên thần và ác quỷ” là một trong những tác phẩm giả tưởng gây tiếng vang đã góp phần đưa tên tuổi Dan Brown ra thế giới. \r\n\r\nKhông theo motif những vụ án mạng thường nhật với động cơ xoay quanh tình hay tiền, cuốn sách khai thác một đề tài khác mới lạ và độc đáo – mâu thuẫn giữa tôn giáo, đại diện là Giáo hội, và khoa học, đại diện là hội kín Illuminati. Dù là người ngoan đạo hay người theo chủ nghĩa vô thần và tin tưởng khoa học tuyệt đối, sau khi đọc xong tác phẩm này, độc giả sẽ phần nào có sự thấu hiểu và cái nhìn thiện cảm hơn với phía đối lập, từ đó hướng đến cuộc sống cân bằng hơn về mặt trí tuệ lẫn tinh thần.\r\n\r\nKhông chỉ tập trung mô tả vụ án và công cuộc phá án, “Thiên thần và ác quỷ” còn cung cấp cho người đọc hiểu biết cơ bản về nhiều lĩnh vực, từ biểu tượng học, lịch sử tôn giáo, nghệ thuật Phục hung đến vật lý lượng tử. Dù “ôm đồm” khá nhiều thứ nặng ký: một cốt truyện trinh thám giả tưởng, một vụ án tầm cỡ vĩ mô, một dàn nhân vật với tiểu sử hùng hậu, một thông điệp sâu sắc, mới mẻ, và bây giờ là cả kho tàng kiến thức về những lĩnh vực nghe qua khá hàn lâm, Dan Brown vẫn hoàn thành xuất sắc từng nhiệm vụ, không vì tập trung khai thác khía cạnh này mà bỏ quên những khía cạnh khác.\r\n\r\nNhư cách mở đầu cổ điển của các tác phẩm trinh thám, quyển tiểu thuyết cũng bắt đầu bằng một vụ án mạng. Leonardo Vetra, nhà vật lý lỗi lạc của Hiệp hội Năng lượng nguyên tử châu Âu (CERN), người đầu tiên thành công trong việc tái tạo phản vật chất, bất ngờ được phát hiện đã chết trong phòng thí nghiệm của chính mình. \r\n\r\nMột vụ án dã man. Lọ phản vật chất đã biến mất. Nhưng đây không chỉ đơn thuần là giết người cướp của. Nạn nhân bị móc một mắt và, khó hiểu hơn là, bị đóng dấu sắt nung lên ngực. Con dấu mang biểu tượng đối xứng huyền thoại của Illuminati – một hội kín tôn thờ khoa học trong quá khứ đã từng bị Giáo hội đàn áp dữ dội và ngỡ như đã tuyệt diệt nhiều thế kỷ.\r\n\r\nCùng lúc đó, ở tòa thánh Vatican, Giáo hoàng đột ngột băng hà, và cả bốn trụ cột – bốn vị Hồng y có khả năng cao nhất sẽ trở thành Giáo hoàng tương lai đều mất tích một cách bí ẩn. \r\n\r\nKhông còn nghi ngờ gì nữa, kẻ thủ ác giấu mặt trong cả hai vụ án là một. Hắn tự nhận mình là truyền nhân còn sót lại của Illuminati và tuyên bố sẽ trả thù cho khoa học, bằng cách khiến Giáo hội phải lụn bại. Từ 8 giờ tối trở đi, cứ mỗi giờ đồng hồ trôi qua, một vị Hồng y sẽ bị giết, và đến 12 giờ đêm, quả bom phản vật chất sẽ phát nổ, đặt dấu chấm hết cho cả thánh địa.\r\n<br>\r\n\r\nTrong tình thế cấp bách ấy, Robert Langdon – giáo sư biểu tượng học nổi tiếng của đại học Harvard – được triệu tập tới Rome, cùng với Vittoria Vetra – con gái nuôi của Leonardo – giải mã những dấu hiệu kẻ ám sát để lại để tìm ra lời giải cho hiểm họa đang treo lơ lửng trên cả thành phố. Liệu Illuminati có thực sự là kẻ đứng sau mọi việc? Khi tấm màn cuối cùng được kéo xuống, một âm mưu kinh hoàng bắt đầu hé lộ.\r\nKịch tính đến phút cuối cùng\r\n<br>\r\n\r\nVũ trụ văn học của Dan Brown luôn có những cú plot twist (tình tiết bất ngờ) được đo ni đóng giày để xuất hiện trong thời điểm thích hợp nhất và chưa bao giờ khiến độc giả thất vọng. Với nghệ thuật dẫn chuyện xuất sắc, dù biết rằng nhất định sẽ có nút thắt ở đâu đó và phải luôn tỉnh táo để nhận biết, người xem vẫn bị lôi cuốn theo mạch truyện, cùng các nhân vật trải qua cuộc chạy đua nghẹt thở với thời gian; hồi hộp dõi theo những dấu hiệu mới của vụ án, cố gắng giải mã chúng mà quên bẵng đi còn có một plot twist vẫn đang chờ đợi. \r\n<br>\r\n\r\nVà rồi khi người đọc nghĩ rằng mình đã dần nắm được những tình tiết quan trọng, thở phào vì câu chuyện cũng đến hồi kết, và chuẩn bị nghe những lời giải thích của kẻ phản diện về động cơ và quá trình gây án của hắn, thì “boom”, mọi thứ đảo lộn hoàn toàn. Dan Brown khiến độc giả tin rằng những nghi ngờ của mình là có cơ sở, và mình đã hoàn toàn hiểu sự việc như nó vốn có, cho đến khi họ đọc những chương cuối cùng và xâu chuỗi lại các sự kiện dưới một góc nhìn khác. \r\n<br>\r\nNhân vật chính không phải là Đấng Toàn năng\r\n<br>\r\n\r\nNghệ thuật xây dựng nhân vật là một điểm nhấn đáng chú ý trong các tác phẩm của Dan Brown nói chung, và trong “Thiên thần và ác quỷ” nói riêng. \r\n<br>\r\n\r\nRobert Langdon – giáo sư biểu tượng học của đại học Harvard – là nhân vật chính xuyên suốt với nhiều ưu điểm nổi bật: ngoại hình sáng sủa, chuyên môn xuất sắc, từng là vận động viên bơi lội ở trường trung học, hiền lành, khiêm nhường và chính trực. \r\n<br>\r\n\r\nNhưng Langdon không hề thông minh và tài giỏi hơn tất cả những người khác, cũng không phải là đấng anh hùng đơn thương độc mã tháo gỡ toàn bộ các nút thắt trong truyện. Các nhân vật phụ hay thứ chính cũng không phải những người mờ nhạt, kỹ năng kém cỏi, thụ động trông chờ vào sự cứu rỗi của nhân vật chính, được thêm vào để tăng số lượng nghi phạm hay làm hoang mang độc giả.\r\n<br>\r\n\r\nMỗi nhân vật, dù đóng vai trò gì trong tác phẩm, đều được tác giả mô tả tỉ mỉ với ngoại hình, cá tính riêng biệt. Họ đều là chuyên gia trong lĩnh vực của mình, và đều xuất chúng chứ không nhạt nhòa như một nhân tố làm nền cho sự phô diễn tài năng của nhân vật chính. \r\n<br>\r\n\r\n\r\nNhư một bức tranh không gian ba chiều, một kịch bản ẩn giấu lập tức hiện ra.\r\n<br>\r\nAi là thiên thần? Và ai mới thực sự là ác quỷ?\r\n<br>\r\nGấp lại quyển sách, người đọc hẳn sẽ còn lưu giữ những ký ức sống động về Kohler, về Olivetti, Ventresca, Mortarti, về Leonardo Vetra, về cố Giáo hoàng, và đặc biệt là về một Vittoria đại diện cho nữ quyền – mạnh mẽ, táo bạo, một người đồng hành thực sự của Langdon, dù không có chuyên môn về biểu tượng học nhưng vẫn tham gia vào công cuộc tìm ra sự thật một cách chủ động và tích cực. Những nhân vật này, dù ít hay nhiều đất diễn, vẫn sẽ để lại một ấn tượng khó quên trong lòng độc giả.\r\n<br>\r\nKhông chỉ thành công trong việc xây dựng dàn nhân vật phụ/thứ chính đa dạng và có ảnh hưởng sâu sắc đến diễn biến truyện, nghệ thuật xây dựng nhân vật chính cũng là một điểm nhấn sáng giá khác của Dan Brown. \r\n<br>\r\nRobert Langdon không phải là kẻ ăn may mà thoát chết hết lần này đến lần khác. \r\n<br>\r\nLúc ông gặp nạn ở thư viện, ở Nhà thờ Lửa và cả ở Đài phun nước, không có ai giúp đỡ ông, cũng không có kỳ tích nào thuộc về ngoại cảnh bất ngờ xảy ra để ông may mắn thoát thân. \r\n<br>\r\nLangdon chỉ có thể tự xoay xở để sinh tồn bằng chính trí tuệ của mình. \r\n<br>\r\nVà sự hiện diện của những đồ vật mà ông dùng để cứu mình khỏi chết trong những tình huống ấy cũng vô cùng hợp lý. Không có bất kỳ vật dụng thừa thãi nào đáng ra không xuất hiện, nhưng lại vô tình được sắp xếp có mặt trong thời điểm đó một cách gượng gạo, để nhân vật có thể bám vào và giải quyết vấn đề một cách chóng vánh. Bằng những tình huống phù hợp để vị giáo sư bộc lộ tài năng, Dan Brown đã thành công thuyết phục người đọc rằng nhân vật này thực sự rất đáng được tuyên dương cho những nỗ lực và sự sáng tạo tuyệt vời, chứ không chỉ là gặp may mắn đơn thuần.\r\n<br>\r\nNhưng Robert Langdon, bất chấp sự thông minh đã bàn đến ở trên, xét cho cùng cũng chỉ là một người bình thường.\r\n<br>\r\nÔng vốn là người của giới học thuật, chưa từng cầm vũ khí, cũng không có bất kỳ kỹ năng chiến đấu nào. Đối mặt với kẻ ám sát dày dạn kinh nghiệm, ông đã phạm phải sai lầm do chính sự thiếu thận trọng của mình. Sai lầm đó không những vô tình khiến cho vụ án nghiêm trọng hơn, mà còn đẩy bản thân Langdon vào tình thế hiểm nghèo.\r\n<br>\r\nHình tượng Robert Langdon được tác giả xây dựng vô cùng hợp lý và không hề bị thổi phồng thành một người có tiềm năng làm được mọi thứ: ông được mắc sai lầm, được thiếu hiểu biết, được sợ hãi, thay vì luôn làm chủ mọi tình huống. Chính những nhược điểm như vậy, được mô tả trong trường hợp riêng biệt để nổi bật lên và không bị lu mờ sau hào quang của những phẩm chất tốt đẹp, đã khiến cho nhân vật trở nên rất thực tế và gần gũi với độc giả.\r\nSự hòa hợp của tôn giáo và khoa học: thông điệp độc đáo và đầy tính nhân văn\r\n<br>\r\nMâu thuẫn giữa tôn giáo và khoa học là vấn đề được nhắc đến xuyên suốt câu chuyện. \r\n<br>\r\nNếu đặt hai phạm trù này lên bàn cân, mỗi người, tùy vào niềm tin cá nhân và hoàn cảnh sống, sẽ ít nhiều đều nghiêng cán cân lệch về một hướng. Và các nhân vật trong “Thiên thần và ác quỷ” cũng có những thiên kiến tương tự. \r\n<br>\r\nNhưng Dan Brown không phán xét ai cả. Mỗi nhân vật, dù là thiên về phía nào, đều có lý lẽ riêng. \r\n<br>\r\nTôn giáo cho con người một đức tin để hướng đến điều thiện, tránh xa cái ác. Khi biết kính sợ một Đấng Toàn năng, người ta sẽ có xu hướng không cho mình cái quyền tự tung tự tác làm càn, vì quả báo sẽ đến dù sớm hay muộn và họ phải chịu sự trừng phạt thích đáng. \r\n<br>\r\nNhưng sùng đạo một cách cực đoan, đến mức xem khoa học là báng bổ thần thánh, trông chờ một vị thần vô hình cứu rỗi khỏi bệnh tật, dẫn đến thiệt hại về mạng người, chính là một tai họa.\r\n<br>\r\nKhoa học cho con người hầu hết những tiện nghi hiện đại. Các loại máy móc, thiết bị công nghệ giải phóng loài người khỏi những công việc vất vả. Sự ra đời của thuốc men, vaccine tiêm ngừa và các trang thiết bị y học đã góp phần cứu chữa vô số nhân mạng. \r\n<br>\r\nNhưng khoa học, cũng như một vị Thần Đèn, quyền năng mà không thể tự định đoạt. Nếu đặt vào nhầm người, khoa học có thể trở thành hơi độc, thành bom nguyên tử – những vũ khí hủy diệt đã gây ra không biết bao nhiêu nạn diệt chủng, thậm chí có thể đưa con người trở về thời kỳ Đồ Đá.\r\n<br>\r\nTác giả để người đại diện của mỗi bên kể câu chuyện của mình. Trong những câu chuyện ấy, độc giả chắc hẳn sẽ thấy mình ở đâu đó. Nhưng hơn cả việc tìm thấy bản thân, người đọc còn được nghe quan điểm từ phía đối diện – những người sinh ra trong môi trường khác, đối mặt với những khó khăn khác trong đời và do vây, hình thành nên tư tưởng hoàn toàn khác biệt. Độc giả có cơ hội nhìn thấy mặt trái trong những điều mình tin tưởng, từ đó có thể cân bằng lại niềm tin của mình để nỗ lực hòa hợp và thấu hiểu nhiều hơn.\r\n<br>\r\nTuy nhiên, dù cho có một vết thương sâu sắc trong đời và không thể cảm thông cho phía đối diện như một số nhân vật trong truyện, con người vẫn có thể quyết định cách hành xử của mình để không làm tổn thương bất kỳ ai. Cùng là chán nản, ghét bỏ và không thể thấu hiểu, nhưng có người chỉ giữ nó trong lòng và tập trung sức lực cho những gì mình tin tưởng, có người lại vì vậy mà hại chết bao nhiêu người vô tội. \r\n<br>\r\nThế nhưng, suy cho cùng, không phải khoa học hay tôn giáo có khả năng gây ra đại họa và phải bị bài trừ, mà chính là thái độ của con người chúng ta đối với những vấn đề ấy. Nếu có những người yếu đuối, mù quáng đến mức tin rằng chỉ cầu xin thần thánh là có thể giải quyết mọi vấn đề trong cuộc sống; có những người lợi dụng sự hiện đại của khoa học để chế tạo vũ khí sát thương hàng loạt; thì chính con người, chứ không phải bất kỳ cái gì khác, mới là kẻ lầm lạc, và đáng bị lên án.\r\n<br>\r\n\r\nTôn giáo và khoa học, liệu có chắc là không thể cùng chung chiến tuyến không? Đó là một vấn đề khác nữa đáng được suy ngẫm thông qua quyển sách. Bỏ qua tất cả những khác biệt bên ngoài, khoa học chính là trí tuệ, và tôn giáo chính là tình thương của con người. Sẽ không hay hơn sao nếu mọi người có thể cân bằng cả hai niềm tin này? Bởi vì họ có thể dùng kiến thức của mình để tạo ra những phát minh khoa học tuyệt vời, và dùng đức tin của mình như một nền tảng đạo đức cho cả việc chế tạo và sử dụng những phát minh ấy. Thế giới sẽ ngày một hiện đại hơn, nhưng song song đó, những giá trị đạo đức cũng sẽ trở thành những giá trị vĩnh cửu.', '2020-06-16', '111.jpg'),
(4, 'Review Phi lý trí – Góc nhìn thú vị về cảm xúc & hành vi trong kinh tế ', '‘Một con lừa đang đói bụng tìm đến một kho thóc để tìm kiếm cỏ khô và phát hiện ra có hai đống cỏ khô kích thước giống hệt nhau ở hai bên của kho thóc. Con lừa đứng giữa hai đống cỏ khô mà không biết chọn đống cỏ nào. Hàng giờ trôi qua mà nó vẫn không thể đưa ra quyết định. Cuối cùng, nó lăn đùng ra chết vì đói.’\r\n<br>\r\nDan Ariely, Phi lý trí –', 'Thật đáng thương cho con lừa! Đầu óc nó đơn giản, vì nó chẳng được Tạo hoá ưu ái ban cho chút lý trí nào để suy nghĩ thấu đáo như loài người. \r\n<br>\r\nCon người quả là một loài động vật kỳ diệu. Nhờ có trí tuệ, chúng ta có khả năng suy nghĩ độc lập, phân tích chi phí – lợi nhuận và dường như luôn đưa ra những quyết định đúng đắn nhất. Ngay cả những pho lý thuyết đồ sộ của kinh tế học truyền thống cũng dựa trên giả định rằng người tiêu dùng là những con người kinh tế – con người duy lý bậc nhất luôn biết tối đa hoá lợi ích cho mình. Vậy nên vấn đề của con lừa kia, vào tay con người chỉ là chuyện nhỏ!\r\n<br>\r\nThật vậy sao?\r\n<br>\r\nNếu biết phần lớn con người đều hành xử giống câu chuyện trên, hẳn là chúng ta sẽ sốc lắm!\r\n<br>\r\nXuất bản năm 2008, ‘Phi lý trí’ trở thành tài liệu kinh tế học kinh điển gây tiếng vang lớn và lọt top sách bán chạy nhất thế giới (best-sellers). Là tập hợp các thí nghiệm và nghiên cứu độc đáo của Dan Ariely – giáo sư kinh tế tại Viện Công nghệ Massachussets (MIT) – quyển sách là một trong những bước đi tiên phong trong việc phổ biến bộ môn kinh tế học hành vi tới đông đảo quần chúng. ‘Phi lý trí’ cung cấp những góc nhìn vô cùng mới mẻ về hành vi con người nói chung và hành vi tiêu dùng nói riêng, vì vậy, không những có thể làm sách gối đầu giường cho bất kỳ ai hoạt động trong ngành kinh doanh và marketing cần đến những kiến thức về tâm lý khách hàng, mà còn phù hợp với tất cả độc giả muốn thấu hiểu sự phi lý trí ‘có hệ thống’ của mình.\r\nKinh tế từ góc nhìn của khoa học hành vi\r\n<br>\r\nKinh tế học hành vi là bộ môn khoa học sinh sau đẻ muộn, tuy nhiên, nó đóng vai trò rất quan trọng công cuộc nghiên cứu kinh tế vi mô (hành vi của từng cá thể trong nền kinh tế).\r\n<br>\r\nPhần lớn độc giả hẳn đã quen với kinh tế học cổ điển, trong đó người tiêu dùng luôn lý trí và đưa ra những quyết định sáng suốt nhằm tối đa hoá lợi nhuận của mình. Điển hình là các quy luật kinh điển liên quan đến cung và cầu. Một phát biểu của quy luật này là khi giá một mặt hàng tăng cao, doanh số sẽ sụt giảm và ngược lại. Kinh tế học truyền thống cho rằng người tiêu dùng có khả năng nhận định giá cả thế nào là hợp lý, từ đó điều chỉnh nhu cầu của họ.\r\n<br>\r\nQuả thật đem đặt lên bàn cân mức giá mới và giá ban đầu là chuyện dễ như ăn kẹo. Nhưng thực ra, vẫn còn một câu hỏi khác. \r\n<br>\r\nCó ai biết mức giá ban đầu thế nào là hợp lý không?\r\n<br>\r\nCâu trả lời trong hầu hết trường hợp là không! Thật đáng ngạc nhiên, con người lý trí trong kinh tế học truyền thống thường không hề có ý niệm nào về giá trị thực của một sản phẩm. Con người này tin vào bất kỳ mức giá nào ngẫu nhiên được đưa ra đầu tiên, và so sánh mọi thứ dựa vào cái “mỏ neo” đó. Chỉ đơn giản bằng việc yêu cầu dùng hai số cuối của thẻ an sinh xã hội để định giá vài món đồ cũ, bạn có thể khiến nhiều người tin rằng con số tùy tiện này chính là giá trị của những món đồ ấy! Vậy nên, quy luật cung cầu, tưởng chừng là cốt lõi của kinh tế học lý trí, lại chỉ đến sau cái nền tảng phi lý trí của người tiêu dùng.\r\n<br>\r\nĐây chỉ là một trong những ví dụ minh họa cho vai trò của hành vi trong kinh tế. Theo truyền thống, con người trong kinh tế học tiêu chuẩn luôn biết rõ mình muốn gì, và tính toán chính xác giá trị của từng lựa chọn. Con người ấy chắc chắn sẽ không bao giờ mua thứ mình không thích và không cần, tiết kiệm đúng theo kế hoạch và không để bản thân bị ảnh hưởng bởi quyết định của người khác. Nhưng qua ‘Phi lý trí’, Dan Ariely đã cung cấp cho độc giả hình ảnh một con người rất khác: một thực thể phức tạp, một sinh vật sống không chỉ có trí tuệ lại mà còn có những hành động rõ rành rành là vô lý mà không hề ý thức được nó. Con người trong kinh tế hành vi luôn có xu hướng bị tác động bởi cảm xúc và môi trường xung quanh. Con người ấy, lúc này hay lúc khác, thường bị chi phối, dẫn dắt và đẩy chệch ra khỏi mục tiêu của mình. \r\n<br>\r\nVới cương vị một giáo sư kinh tế học hành vi, tác giả dẫn người đọc đi xuyên qua những lý thuyết vững chắc của kinh tế cổ điển, dùng đèn soi rọi tất cả những lỗ hổng ẩn náu, chỉ ra những góc nhìn thú vị và khiến độc giả bật cười khi nhận ra những hành vi rất chi là vô lý của mình. Trong suốt chuyến hành trình ấy, người đọc không chỉ khám phá ra câu trả lời cho những câu hỏi nhỏ thú vị được nêu trong quyển sách, mà còn cho những vấn đề ở tầm vĩ mô hơn rất nhiều, thông qua phần bàn luận mở rộng ở mỗi chương của Dan Ariely. Như ông đã trình bày, hiểu được động lực đằng sau mỗi lựa chọn cho ta nền tảng để nhận biết tâm lý khách hàng trong các lĩnh vực từ ăn uống, giải trí đến đầu tư, tính hiệu quả của chi phí y tế, những vụ tham nhũng nơi công sở, việc tiết kiệm tiền và hằng hà sa số những vấn đề quan trọng khác.\r\nSinh động và khơi gợi cảm hứng\r\n<br>\r\n“Phi lý trí” là một cuốn sách nhập môn. Vì vậy, những kiến thức trong quyển sách luôn được trình bày một cách hấp dẫn, lôi cuốn và sống động, phù hợp với đại đa số quần chúng. Tác giả thường bắt đầu mỗi chương sách bằng một câu hỏi độc đáo, khơi gợi sự tò mò của độc giả. \r\n<br>\r\n\r\nBạn đã bao giờ giành lấy tấm phiếu MUA HÀNG MIỄN PHÍ một gói cà phê dù bạn không uống và không có cả máy pha cà phê?\r\n<br>\r\nTại sao chúng ta vui mừng khi làm một việc gì đó, nhưng lại không vui khi được trả tiền để làm việc đó?\r\n<br>\r\nTại sao một viên aspirin 50 xu có thể làm được điều mà một viên aspirin 1 xu không thể làm được?\r\n<br>\r\nTrên đây chỉ là 3 trong số những câu hỏi về sự “quái lạ” trong hành vi của con người. Bằng cách đặt câu hỏi ở đầu chương, Dan Ariely vừa khiến người đọc háo hức muốn biết điều gì sẽ xảy ra tiếp theo, vừa tóm tắt nhanh gọn chủ đề của cả chương sách. \r\n<br>\r\nKhông chỉ đặt những câu hỏi thú vị, tác giả còn khéo léo lồng ghép những thí nghiệm khoa học vào từng vấn đề đang được bàn đến. “Phi lý trí” là một cuốn sách khoa học kinh tế hẳn hoi, nhưng không hề khô khan với hàng mớ lý thuyết khiến độc giả đau đầu, mà cực kỳ dễ nắm bắt qua những nghiên cứu được thực hiện bởi giáo sư Dan Ariely và các đồng sự. Chỉ bằng cách đọc qua những thí nghiệm đơn giản và kết quả thu được, người đọc có thể hình dung ngay lập tức nguyên lý ẩn sau đó mà gần như chẳng cần phải học quá nhiều thuật ngữ kinh tế. \r\n<br>\r\nVà để cho quyển sách thêm phần thú vị, tác giả chẳng ngần ngại đem những câu chuyện độc lạ từ ngụ ngôn đến thực tế cuộc sống để minh họa cho những lý thuyết được nói đến (chuyện con lừa đã kể ở đầu bài, chuyện những con ngỗng nhận một nhà khoa học làm mẹ (!), chuyện cô sinh viên và hai cậu bạn trai, chuyện anh chàng trả tiền cho bữa tối của mẹ vợ, v.v.) Mỗi câu chuyện kể trên đều có vẻ tức cười nhưng lại cực kỳ hữu ích cho việc khám phá những nguyên lý phức tạp của kinh tế vi mô được đề cập đến trong quyển sách.\r\n<br><br><br>', '2020-06-16', '333.jpg'),
(5, 'Review sách Đường Còn Dài, Còn Dài – Free your mind, and the rest will follow', 'Nguyễn Thiên Ngân ra mắt truyện dài đầu tay vào năm 2009 với tựa đề “Đường còn dài, còn dài” – viết về chuyến du lịch bụi từ Nam chí Bắc rồi trở về Nam nhưng lại không phải thể loại du kí, mà ngợp hơi thở của những lênh đênh tuổi trẻ rất đời, với những tâm sinh lý rối bời về tình yêu, học hành, gia đình, sự nghiệp. Tập sách mỏng viết cho ngưỡng cửa 20, muốn nhìn ra thế giới và nhìn sâu vào lòng mình.', 'Nguyễn Thiên Ngân chỉ mất 2 tuần trong tháng 11 năm 2008 để hoàn thành câu chuyện này khi chị vừa tròn tuổi 20, tác phẩm bởi vậy mà căng tràn thanh xuân nồng nàn của tuổi trẻ, chân phương và khá non nớt.\r\n<br>\r\n10 năm sau ngoảnh lại, chị nhận xét về truyện dài đầu tay của mình:\r\n<br>\r\n“Cuốn đó bây giờ đọc lại thấy nhiều chỗ còn ngây ngô lắm. Nhưng nếu có cơ hội được sửa, mình cũng không muốn sửa. Mỗi quãng đời có phận sự của nó. Và mình tôn trọng cái thời sinh viên loay hoay, nhiều băn khoăn sợ hãi ấy.”\r\nKhởi đầu chuyến đi nghe nhẹ hều, mà thực sự có ơ hờ như lời nói đó không? – Khi một sinh viên năm ba quyết định bỏ học, gặp một tay lập trình viên quyết định nghỉ làm rồi hùa nhau cùng đi du lịch bụi? – Nghỉ luôn. Chấm dứt! – Chuyến hành trình không quan tâm tối nay ngủ ở đâu và sáng ngày mai ăn gì, không kế hoạch, không điểm cuối…\r\n<br>\r\n“Đường còn dài, còn dài” tựu chung không phải là một câu chuyện buồn, thông điệp truyền tải cũng khá lạc quan, nhưng mỗi nhân vật của nó đều ì ạch trên vai những trăn trở và chán nản trong cuộc sống thực tại.\r\n<br>\r\nN., cậu sinh viên khoa Văn, 20 tuổi, bỏ học, đi phượt khắp nẻo đường đất nước trong trạng thái vô định. Sói, tay lập trình viên tóc dài, kiệm lời, bỏ việc, rong ruổi. Damien, gã trai Tây tốt nghiệp ngành Sinh học, lười đi làm, lăn lộn qua bao nhiêu nước. Chiêu Anh, cô gái mang vấn đề về tâm lý, với những lo lắng, ảo tưởng và cuồng loạn. Trân, con bé nằng nặc ra Hà Nội học vì lỡ mê một chú phóng viên ảnh lớn hơn chục tuổi, quan trọng là, sắp có vợ. Ni, biên tập viên, 28 tuổi, bỏ việc ở Sài Gòn, chọn Đà Lạt làm chốn dừng chân và mở quán cafe nhỏ đặt tên là “Dream a little dream”. Ông M., họa sĩ Sài Gòn đã góa vợ, lên Đà Lạt để yên tĩnh sáng tác, và rồi say mê Ni, mối tình đơn phương của một lão già si.\r\n<br>\r\nMà kể cũng lạ, N. và M. – một nhân vật chính xuyên suốt tác phẩm và một nhân vật phụ của phụ – chỉ hai nhân vật này là không có tên cụ thể. Tại sao nhỉ?\r\n<br>\r\nDù sao thì, có lẽ, chỉ có nàng Francessca màu vàng đỏm dáng là mang tâm thái lạc quan yêu đời nhất tác phẩm. Vì đơn giản, đồ vật thì vô tri.\r\nChuyến hành trình 9 tháng trên 160 trang giấy – chóng vánh đến hoảng hốt, nhanh tựa một giấc mơ – mà cũng có thể là mơ lắm! Kết cấu đầu cuối tương ứng trên chuyến xe buýt S09 khiến chuyến đi định hình rõ ràng, để thấy rằng vứt bỏ tất cả và lên đường cũng không có gì là kinh thiên động địa lắm!\r\n<br>\r\nVì đường còn dài, còn dài.\r\n<br>\r\nMệt mỏi quá thì cứ đi, cho bản thân chút thời gian sắp xếp lại đống tơ vò trong lòng, rồi về hoặc không, nhưng chí ít đã thảnh thơi nghĩ về quá khứ, đã yêu thêm hiện tại và đã thấy mờ tỏ về tương lai.\r\n<br>\r\nVì đời còn dài, còn dài.\r\n<br>\r\nÔi! Tuổi trẻ!\r\n<br>\r\nN. gọi sinh viên là cái “nghề”. Bỏ học – không làm “nghề” sinh viên nữa – dẫu biết như vậy là bất hiếu. N. ví mình như người đứng trên thành cầu, ngắm xe cộ lại qua và chưa cả kịp nghĩ là nên nhảy xuống hay không thì đã thấy bản thân rơi rồi! – Có thể tự nhảy, có thể trượt chân, có thể bị ai đó xô – nhưng lúc ý thức được thì thấy mình đã rớt xuống nước rồi!\r\n<br>\r\nVà N., không biết bơi.\r\n<br>\r\nMột ví dụ quá tượng hình cho tuổi 20 đầy bồng bột, đầy yếu mềm, cũng đầy hoang mang và lo sợ.\r\n<br>\r\nPhải chăng tuổi 20 của ai cũng như vậy? Cũng bị mắc kẹt trong sự luẩn quẩn, không bị gì cản lại nhưng vẫn cứ đi vòng tròn? Cứ suy nghĩ nhiều mà cũng chỉ dừng lại ở nghĩ suy? Mà mệt cái là, càng nghĩ càng rối, rối chuyện học và rối cả chuyện yêu, chuyện cuộc đời, chuyện tương lai xa vời như không phải của mình, không nắm bắt được. Chơi vơi.\r\n<br>\r\nSói gọi N. là “thằng tiểu thư èo uột hay suy gẫm, dẫu trải nghiệm về cuộc đời chưa đựng đầy một vốc tay”.\r\n<br>\r\nSói viết bâng quơ trong email là thế, nhưng đã tỏ rõ vấn đề của N. – thiếu trải nghiệm, nên suy ngẫm hoài vẫn không tìm được hướng đi.\r\n<br>\r\nVậy thì đừng nghĩ nữa, mà hãy đi!\r\n<br>\r\nĐi thật. Đi theo nghĩa đen. Đi để nhìn ra thế giới. Đi để trải nghiệm, va chạm. Đi để nhìn sâu vào lòng mình. Rốt cuộc thì, vốn liếng của tuổi trẻ chính là tuổi trẻ. Đi để học từ thất bại. Đi, vì đường còn dài, còn dài.\r\n<br><br><br>', '2020-06-16', '444.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `status` tinyint(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `order`
--

INSERT INTO `order` (`id`, `transaction_id`, `product_id`, `qty`, `amount`, `status`) VALUES
(1, 3, 21, 1, '200000', 0),
(2, 3, 15, 1, '15000', 0),
(3, 4, 18, 1, '200000', 0),
(4, 5, 21, 1, '200000', 0),
(5, 6, 21, 4, '800000', 0),
(6, 7, 21, 4, '800000', 0),
(7, 7, 19, 1, '20000', 0),
(8, 8, 20, 1, '570000', 0),
(9, 8, 14, 1, '100000', 0),
(10, 9, 19, 10000, '200000000', 0),
(11, 10, 19, 1, '20000', 0),
(12, 11, 19, 10, '200000', 0),
(13, 11, 18, 1, '200000', 0),
(14, 12, 18, 5, '1000000', 0),
(15, 13, 21, 1, '190000', 0),
(16, 14, 20, 6, '2736000', 0),
(17, 15, 36, 10, '36400', 0),
(18, 15, 38, 1, '36400', 0),
(19, 16, 36, 1, '20930', 0),
(20, 17, 26, 10, '1900000', 0),
(21, 18, 24, 10, '1800000', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `discount` int(11) NOT NULL,
  `image_link` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `view` tinyint(4) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `buyed` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `created` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`id`, `category_id`, `name`, `price`, `discount`, `image_link`, `view`, `content`, `buyed`, `total`, `created`) VALUES
(15, 7, 'bộ sách giáo khoa lớp 8', '200000', 4, '8.jpg', 1, 'Sách giáo khoa lớp 8, chương trình chuẩn của bộ giáo dục VN, bộ sách bài học *6 cuốn*, bộ bài tập *9 cuốn* Sách theo niên học 2017-2018 Sách tham khảo, sách nâng cao toán ', 0, 50, '2020-06-16'),
(16, 7, 'bộ sách giáo khoa lớp 7', '250000', 9, '7.jpg', 8, 'Sách giáo khoa lớp 7, chương trình chuẩn của bộ giáo dục VN, bộ sách bài học *6 cuốn*, bộ bài tập *9 cuốn* Sách theo niên học 2017-2018 Sách tham khảo, sách nâng cao toán ', 0, 100, '2020-06-16'),
(23, 7, 'bộ sách giáo khoa lớp 2', '250000', 10, '2.jpg', 3, 'Sách giáo khoa lớp 2, chương trình chuẩn của bộ giáo dục VN, bộ sách bài học *6 cuốn*, bộ bài tập *9 cuốn* Sách theo niên học 2017-2018 Sách tham khảo, sách nâng cao toán ', 0, 100, '2020-06-16'),
(18, 7, 'bộ sách giáo khoa lớp 6', '250000', 8, '6.jpg', 3, 'Sách giáo khoa lớp 6, chương trình chuẩn của bộ giáo dục VN, bộ sách bài học *6 cuốn*, bộ bài tập *9 cuốn* Sách theo niên học 2017-2018 Sách tham khảo, sách nâng cao toán ', 0, 133, '2020-06-16'),
(40, 1, 'bút máy ', '200000', 5, '909fb_25_1.jpg', 0, 'but viết', 0, 50, '2020-06-27'),
(21, 7, 'bộ sách giáo khoa lớp 3', '250000', 5, '3.jpg', 25, 'Sách giáo khoa lớp 3, chương trình chuẩn của bộ giáo dục VN, bộ sách bài học *6 cuốn*, bộ bài tập *9 cuốn* Sách theo niên học 2017-2018 Sách tham khảo, sách nâng cao toán ', 0, 100, '2020-05-06'),
(24, 7, 'bộ sách giáo khoa lớp 1', '200000', 10, '1.jpg', 1, 'Sách giáo khoa lớp 1, chương trình chuẩn của bộ giáo dục VN, bộ sách bài học *6 cuốn*, bộ bài tập *9 cuốn* Sách theo niên học 2017-2018 Sách tham khảo, sách nâng cao toán ', 0, 100, '2020-06-16'),
(25, 7, 'bộ sách giáo khoa lớp 10', '250000', 9, '1010.jpg', 0, 'Sách giáo khoa lớp 10, chương trình chuẩn của bộ giáo dục VN, bộ sách bài học *6 cuốn*, bộ bài tập *9 cuốn* Sách theo niên học 2017-2018 Sách tham khảo, sách nâng cao toán ', 0, 199, '2020-06-16'),
(26, 7, 'bộ sách giáo khoa lớp 11', '200000', 5, '11.jpg', 0, 'Sách giáo khoa lớp 11, chương trình chuẩn của bộ giáo dục VN, bộ sách bài học *6 cuốn*, bộ bài tập *9 cuốn* Sách theo niên học 2017-2018 Sách tham khảo, sách nâng cao toán ', 0, 100, '2020-06-16'),
(27, 7, 'bộ sách giáo khoa lớp 12', '200000', 3, '12.jpg', 2, 'Sách giáo khoa lớp 12, chương trình chuẩn của bộ giáo dục VN, bộ sách bài học *6 cuốn*, bộ bài tập *9 cuốn* Sách theo niên học 2017-2018 Sách tham khảo, sách nâng cao toán ', 0, 144, '2020-06-16'),
(29, 1, 'Bút Dạ Quang OT-HL0005BU - Xanh Dương', '8500', 9, 'b2.jpg', 1, 'Nhà cung cấp:Công Ty TNHH Headfully\r\nThương hiệu:HooHooHaHa®\r\nXuất xứ:Việt Nam\r\nNơi sản xuất:Trung Quốc ', 0, 109, '2020-06-16'),
(31, 1, 'Bút Gel Crown MTJ-500 0.4mm - Mực Xanh ', '10000', 9, 'b4.jpg', 0, 'Nhà cung cấp:Cty TNHH XNK VASAGO\r\n<br>\r\nThương hiệu:Crown\r\n<br>\r\nXuất xứ:Thương Hiệu Hàn Quốc\r\n<br>\r\nNơi sản xuất:Hàn Quốc', 0, 100, '2020-06-16'),
(33, 1, 'Bút Dạ Quang OT-HL0005Pk - Màu Hồng', '8500', 9, 'b6.jpg', 1, 'Nhà cung cấp:Cty TNHH XNK VASAGO\r\n<br>\r\nThương hiệu:Crown\r\n<br>\r\nXuất xứ:Thương Hiệu Hàn Quốc\r\n<br>', 0, 66, '2020-06-16'),
(34, 1, 'Bút Lông Kim Artline Supreme', '10000', 7, 'b7.jpg', 0, 'bút mới', 0, 80, '2020-06-16'),
(36, 1, 'Viết Chì Bấm Staedtler 777 05-5 X.Đọt Chuối', '23000', 9, 'b9.jpg', 7, 'hàng chất lượng', 0, 80, '2020-06-16'),
(38, 1, 'Bộ Sản Phẩm Ghi Nhớ Memorization Kit', '80000', 20, 'b11.jpg', 2, '100', 0, 90, '2020-06-16');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `slide`
--

CREATE TABLE `slide` (
  `id` int(11) NOT NULL,
  `image_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `slide`
--

INSERT INTO `slide` (`id`, `image_link`, `link`) VALUES
(1, 'slide2.jpg', '#'),
(2, '60816552760724101.jpg', '#'),
(4, 'chỉ_mục.jpg', '#'),
(6, 'hinh.jpg', '#');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `transaction`
--

CREATE TABLE `transaction` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `payment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_info` text COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `transaction`
--

INSERT INTO `transaction` (`id`, `user_id`, `user_name`, `user_email`, `user_phone`, `amount`, `payment`, `payment_info`, `message`, `status`, `created`) VALUES
(3, 1, 'nguyễn tấn phước', 'nguyenhadong3333@gmail.com', '2147483647', '215000', 'tainha', '', 'fdfdfdf', 0, 1589038297),
(4, 1, 'nguyễn tấn phước', 'nguyenhadong3333@gmail.com', '2147483647', '200000', 'nganluong', '', 'tổ 6 ấp 1', 0, 1589038444),
(5, 1, 'nguyễn tấn phước', 'nguyenhadong3333@gmail.com', '2147483647', '200000', 'nganluong', '', 'fdfdfdf', 0, 1589038536),
(6, 1, 'nguyễn tấn phước', 'nguyenhadong3333@gmail.com', '2147483647', '820000', 'nganluong', '', 'uiuiuiui', 0, 1589039671),
(7, 1, 'nguyễn tấn phước', 'nguyenhadong3333@gmail.com', '2147483647', '820000', 'nganluong', '', 'uyuyu', 0, 1589039717),
(8, 9, 'nguyên thành nguyên', 'thanhnguyen1@gmail.com', '2147483647', '670000', 'tainha', '', 'tổ 6 khu phố 3', 0, 1590398804),
(9, 9, 'nguyên thành nguyên', 'thanhnguyen1@gmail.com', '2147483647', '200000000', 'tainha', '', 'tokokoko', 0, 1590398919),
(10, 9, 'nguyên thành nguyên', 'thanhnguyen1@gmail.com', '2147483647', '20000', 'nganluong', '', 'rêrer', 0, 1590404347),
(11, 0, 'nguyễn băn a', 'wwedffffgg4dwdfd@gmail.com', '967990109', '390000', 'nganluong', '', 'xcxcxcxcxc', 0, 1590405075),
(12, 0, 'bút mực nước', 'dsdsds344dsw@gmail.com', '4545454545', '970000', 'nganluong', '', '4544545fdf', 0, 1590405196),
(13, 0, 'nguyễn hoài nam', 'nguyertrtrnhadonggfgfg3333@gmail.com', '0967888333', '190000', 'tainha', '', 'rtrtrtrt', 0, 1590405431),
(14, 0, 'nguyễn tấn phước', 'wwedwe444444444dwdfdfvvbbbew@gmail.com', '4444444', '2736000', 'tainha', '', 'iuiuiuiu', 0, 1592010175),
(15, 0, 'nguyễn đình thi', 'dinhthi@gmail.com', '0967990105', '245700', 'tainha', '', 'tổ 5 ap 6 long thanh', 0, 1592316935),
(16, 0, 'nguyễn tấn phước', 'thanhnguyen1@gmail.com', '0967990105', '20930', 'tainha', '', 'll', 0, 1592362277),
(17, 0, 'huyenh tram', 'nguyenhadong3333@gmail.com', '4545454545', '1900000', 'tainha', '', '23232', 0, 1593091709),
(18, 0, 'nguyên thành nguyên', 'nguyenhadong3333@gmail.com', '0967990105', '1800000', 'tainha', '', 'to 4 ap 6', 0, 1593244731);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(15) NOT NULL,
  `created` int(11) NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `phone`, `created`, `address`) VALUES
(1, 'nguyễn tấn phước', 'nguyenhadong3333@gmail.com', '25d55ad283aa400af464c76d713c07ad', 2147483647, 0, ''),
(2, 'nguyễn tấn phước', 'nguyenhadonggfgfg3333@gmail.com', '25d55ad283aa400af464c76d713c07ad', 2147483647, 0, ''),
(3, 'nguyễn tấn phước', 'nguyenhadeeeeeong@gmail.com', '25d55ad283aa400af464c76d713c07ad', 967990105, 0, ''),
(4, 'nguyễn tấn phúc', 'dsdsdsdsw@gmail.com', '25d55ad283aa400af464c76d713c07ad', 4444444, 0, ''),
(5, 'nguyễn tấn lộc', 'nguyentanloc@gmail.com', '25f9e794323b453885f5181f1b624d0b', 967888333, 0, ''),
(6, 'nguyễn hà nam', 'hanam@gmail.com', '25d55ad283aa400af464c76d713c07ad', 967888333, 0, ''),
(7, 'nguyễn anh tuấn', 'hoainam@gmail.com', '25d55ad283aa400af464c76d713c07ad', 967990109, 0, 'tổ 6 khu 1'),
(8, 'nguyên thành nguyên', 'thanhnguyen@gmail.com', '25d55ad283aa400af464c76d713c07ad', 967990105, 0, 'tổ 6 khu phố 4'),
(9, 'nguyên thành nguyên', 'thanhnguyen1@gmail.com', '25f9e794323b453885f5181f1b624d0b', 2147483647, 0, '128 hà huy giap Q.12 phường thạnh xuân'),
(10, 'nguyen nam', 'nguyenhadong000@gmail.com', '25f9e794323b453885f5181f1b624d0b', 967888333, 0, 'tổ 6 khu phố 9'),
(11, 'nguyễn van a', 'vana@gamil.com', '54e1ffbec1e8dfbfd2b852c1e4553e12', 967990105, 0, 'tổ 6 khu phố 4');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `gioithieu`
--
ALTER TABLE `gioithieu`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `logo`
--
ALTER TABLE `logo`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `gioithieu`
--
ALTER TABLE `gioithieu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `logo`
--
ALTER TABLE `logo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT cho bảng `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT cho bảng `slide`
--
ALTER TABLE `slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
