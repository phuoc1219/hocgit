<?php
Class MY_Controller extends CI_Controller
{
    //bien gui du lieu sang ben view
    public $data = array();
    
    function __construct()
    {
        //ke thua tu CI_Controller
        parent::__construct();
        //lay danh sach danh muc san pham
        $controller = $this->uri->segment(1);
        switch ($controller)
        {
            case 'admin' :
                {
                    //xu ly cac du lieu khi truy cap vao trang admin
                   
                    $this->kiem_tra_login();
                    break;
                }
            default:
                {
                    //xử lý dữ liệu ngoài giao diện
                    $this->load->model('Danh_Muc_Model');
                    $input = array();
                    $input['where'] = array('parent_id' => 0);
                    $catelog = $this->Danh_Muc_Model->get_Danh_Sach($input);
                    foreach ($catelog as $row)
                    {
                        $input['where'] = array('parent_id' => $row->id);
                        $subs = $this->Danh_Muc_Model->get_Danh_Sach($input);
                        $row->subs = $subs;
                    }
                    
                    $this->data['catelog'] = $catelog;
                    
                    //kiem tra xem thanh vien da dang nhap hay chua
                    $user_id_login = $this->session->userdata('user_id_login');
                    $this->data['user_id_login'] = $user_id_login;
                    //neu da dang nhap thi lay thong tin cua thanh vien
                    if($user_id_login)
                    {
                        $this->load->model('user_model');
                        $user_info = $this->user_model->get_Thong_Tin($user_id_login);
                        $this->data['user_info'] = $user_info;
                    }
                    
                    //gọi thư viện cart
                    $this->load->library('cart');
                    $this->data['tongso']=$this->cart->total_items();

                     
                        // lấy danh sách liên hệ
                        $this->load->model('Lien_He_Model');
                        $input=array();
                        $DanhSach=$this->Lien_He_Model->get_Danh_Sach($input);
                        
                        $this->data['list1']=$DanhSach;

                        // lấy  logo
                        $this->load->model('logo_Model');
                        $input=array();
                        $DanhSach=$this->logo_Model->get_Danh_Sach($input);
                        
                        $this->data['list2']=$DanhSach;
       
                }
                
        }
      
       
      
       
          
       
      
        
     
    }
      
    private  function kiem_tra_login()
    {
        $controller =$this->uri->segment(2);
        $user= $this->session->userdata('login');;
        if($controller != 'login' && !$user)
        {
            redirect(base_url('admin/login/index'));
        }
        
        if($controller == 'login' && $user)
        {
            redirect(base_url('admin/home/index'));
        }
    }
    
    
   
       
       
}


