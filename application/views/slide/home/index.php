

<!DOCTYPE html>
<html lang="zxx">
   <head>
      <?php $this->load->view('slide/head'); ?>
   </head>
   <body>
      <!-- Header Section Begin -->
      <header class="header">
         <?php $this->load->view('slide/header',$this->data);?>
      </header>
      <!-- Header Section End -->
      <!-- Hero Section Begin -->
      <section class="hero">
         <div class="container-fluid">
            <div class="row">
               <div class="col-lg-3">
                  <div class="hero__categories">
                     <div class="hero__categories__all">
                        <i class="fa fa-bars"></i>
                        <span>Danh Mục Sản Phẩm</span>
                     </div>
                     <ul>
                     <?php foreach ($catelog_sp as $key): ?>
                        <li>
                           <a href="<?php echo base_url('san_pham_controller/danhmuc/' .$key->id) ?>"><?php echo $key->name ?> <i class="fa fa-angle-right"></i></a>
                           <?php if(!empty($key->subs)): ?>
                           <ul class="sub-menu">
                           <?php foreach ($key->subs as $danhmuccon): ?>
                              <li><a href="<?php echo base_url('san_pham_controller/danhmuc/' .$danhmuccon->id) ?>"><?php echo $danhmuccon->name ?></a></li>
                            
                              <?php endforeach; ?>
                           </ul>
                           <?php endif; ?>
                        </li>
                        <?php endforeach; ?>
                       
                     </ul>
                  </div>
               </div>
               <div class="col-lg-9">
                  <div class="hero__search">
                     <div class="hero__search__form">
                        <form action="<?php echo base_url('san_pham_controller/timkiem') ?>" method="get">
                           <input type="text" placeholder="Tìm Kiếm" name="timkiem" />
                           <button type="submit" class="site-btn">Tìm Kiếm</button>
                        </form>
                     </div>
                     <div class="hero__search__phone">
                        <div class="hero__search__phone__icon">
                           <a href="<?php echo base_url('cart') ?>"> <i class="fa fa-cart-plus"></i></a>
						  
                        </div>
                        <div class="hero__search__phone__text">
                           <a href="<?php echo base_url('cart') ?>">
                              <h6>Giỏ Hàng</h6>
                           </a>
                           <a href="<?php echo base_url('cart') ?>">
                           <span>Số Lượng: <b><?php echo $tongso_sp ?></b></span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <div id="myCarousel" class="carousel slide" data-ride="carousel">
                     <!-- Indicators -->
                     <ol class="carousel-indicators">
                     <?php $i=0 ?>
                     <?php foreach( $list_hinh as $key): ?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $i ?>" 
                        
                        <?php if($i==0):?>
                           class="active"
                        <?php endif; ?> 
                        
                        
                        ></li>
                        <?php $i++; ?>
                     <?php endforeach; ?>
                       
                     </ol>
                     <!-- Wrapper for slides -->
                     <div class="carousel-inner">
                     <?php $i=0; ?>
                     <?php foreach( $list_hinh as $key): ?>
                        <div 
                           <?php if($i == 0):?>
                        class="item active"
                           <?php else: ?>
                           class="item"
                           <?php endif; ?>
                        >
                        <?php $i++; ?>



                           <img id="img-index" src="<?php echo base_url('upload/hinh_chay/'.$key->image_link ) ?>" alt="Los Angeles"  />
                        </div>
                     <?php endforeach; ?>
                     </div>
                     <!-- Left and right controls -->
                     <a class="left " href="#myCarousel" data-slide="prev">
                     <span class="glyphicon glyphicon-chevron-left"></span>
                     <span class="sr-only">Previous</span>
                     </a>
                     <a class="right" href="#myCarousel" data-slide="next">
                     <span class="glyphicon glyphicon-chevron-right icon-right"></span>
                     <span class="sr-only">Next</span>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Hero Section End -->
	  
	  
	  
	  
	  <script>
  $(document).ready(function(){
    $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
     autoplay: true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})
});
</script>
	  
	  
	  
	  
	  
	  
	  

	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
      <!-- Quảng cáo giao hàng-->
      <div class="container-fluid" >
         <div class="col-sm-4 giaohang" >
            <p><i class="fas fa-clock"></i> <b> Giao Hàng Nhanh</b></p>
         </div>
         <div class="col-sm-4 giaohang" >
            <p><i class="fas fa-truck"></i> <b> Miễn Phí Giao Hàng</b></p>
         </div>
         <div class="col-sm-4 giaohang" >
            <p><i class="fas fa-sync-alt"></i> <b> Đổi Trả Nhanh Chóng</b></p>
         </div>
      </div>
      <!-- end quảng cáo giao hàng -->
      <!--slide chạy sản phẩm -->
      
      <div class="flashsale-header container-fluid">
         <a href="<?php echo base_url('sp_index/sp_moi') ?>"><span class="flashsale-header-title">Sản Phẩm Mới Nhất</span></a>
         <a href="<?php echo base_url('sp_index/sp_moi') ?>"><span style="margin-left:950px;color:white"> < xem tất cả ></span></a>
      </div>
      <div class="container-fluid sp-slide">
         	  
            
            <div class="owl-carousel owl-theme">
               <?php foreach ($list as $key): ?>   
               <div class="col-md-10 col-sm-6">
                     <div class="product-grid2">
                        <div class="product-image2">
                           <a href="#">
                                 <img class="pic-1" style="height:250px;" src="<?php echo base_url('upload/san_pham/' .$key->image_link) ?>">

                           </a>
                           <ul class="social">
                                 <li><a href="<?php  echo base_url('san_pham_controller/detail/'.$key->id)?>" data-tip="Chi Tiết"><i class="fa fa-eye"></i></a></li>
                                 <li><a href="<?php echo base_url('cart/them/'.$key->id) ?>" data-tip="Thêm Vào Giỏ"><i class="fa fa-shopping-cart"></i></a></li>
                           </ul>
                           <?php if($key->discount >0):?>
                           <span style="position:relative;z-index:10;top:-250px;background:green;color:white;padding:5px" >-<?php  echo $key->discount ?>%</span>
                           <?php else: ?>
                              <span style="position:relative;z-index:10;top:-250px;background:green;color:white;padding:5px;opacity: 0;" ></span>
                           <?php endif; ?>
                        </div>
                        <div class="product-content">
                           <h3 class="title" style="height:60px; overflow: hidden;" ><a href="<?php  echo base_url('san_pham_controller/detail/'.$key->id)?>"><?php  echo $key->name ?></a></h3>
                           <?php if($key->discount>0): ?>
        			            <?php $kq=100 - $key->discount; $giacu=$key->price; $giamoi= $giacu*($kq/100);?>
                           <span class="price" style="font-size:16px"><b style="background:green;color:white"><?php   echo number_format($giamoi)?>đ</b>-<b style="text-decoration: line-through;"><?php   echo number_format($giacu)?>đ</b></span>
                           <?php else: ?>
                           <span class="price" style="font-size:16px"><b style="background:green;color:white"><?php   echo number_format($giamoi)?>đ</b></span>
                           <?php endif; ?>
                        </div>
                     </div>
               </div>
               <?php endforeach; ?> 
              
            </div>
      </div>
     
      <div class="flashsale-header container-fluid">
         <a href="<?php echo base_url('sp_index/sp_KM') ?>"><span class="flashsale-header-title">Sản Phẩm Khuyến Mãi</span></a>
         <a href="<?php echo base_url('sp_index/sp_KM') ?>"><span style="margin-left:920px;color:white">< xem tất cả ></span></a>
      </div>
      <div class="container-fluid sp">
      <?php foreach ($KM as $key): ?>   
               <div class="col-md-2 col-sm-6 sp_dis" style="margin-bottom:20px;">
                     <div class="product-grid2">
                        <div class="product-image2">
                           <a href="#">
                                 <img class="pic-1" style="height:250px;" src="<?php echo base_url('upload/san_pham/' .$key->image_link) ?>">

                           </a>
                           <ul class="social">
                                 <li><a href="<?php  echo base_url('san_pham_controller/detail/'.$key->id)?>" data-tip="Chi Tiết"><i class="fa fa-eye"></i></a></li>
                                 <li><a href="<?php echo base_url('cart/them/'.$key->id) ?>" data-tip="Thêm Vào Giỏ"><i class="fa fa-shopping-cart"></i></a></li>
                           </ul>
                           <?php if($key->discount >0):?>
                           <span style="position:relative;z-index:10;top:-250px;background:green;color:white;padding:5px;" >-<?php  echo $key->discount ?>%</span>
                           <?php else: ?>
                              <span style="position:relative;z-index:10;top:-250px;background:green;color:white;padding:5px;opacity: 0;" ></span>
                           <?php endif; ?>
                        </div>
                        <div class="product-content">
                           <h3 class="title" style="height:60px; overflow: hidden;" ><a href="<?php  echo base_url('san_pham_controller/detail/'.$key->id)?>"><?php  echo $key->name ?></a></h3>
                           <?php if($key->discount>0): ?>
        			            <?php $kq=100 - $key->discount; $giacu=$key->price; $giamoi= $giacu*($kq/100);?>
                           <span class="price" style="font-size:16px"><b style="background:green;color:white"><?php   echo number_format($giamoi)?>đ</b>-<b style="text-decoration: line-through;"><?php   echo number_format($giacu)?>đ</b></span>
                           <?php else: ?>
                           <span class="price" style="font-size:16px"><b style="background:green;color:white"><?php   echo number_format($giamoi)?>đ</b></span>
                           <?php endif; ?>
                        </div>
                     </div>
               </div>
               
               <?php endforeach; ?> 

      </div>
      

      <!-- Footer Section Begin -->
      <footer class="footer spad">
      <?php $this->load->view('slide/footer') ?>
      </footer>
      <!-- Footer Section End -->
   </body>
</html>

