<div class="container wrapper">
            <div class="row cart-head">
                <div class="container">
               
                </div>
            </div>    
            
            <div class="row cart-body">
                <form class="form-horizontal" method="post" action="">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-push-6 col-sm-push-6">
                    <!--REVIEW ORDER-->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            Đơn Hàng <div class="pull-right"><small><a class="afix-1" href="#">Chỉnh Sữa Giỏ Hàng</a></small></div>
                        </div>
                        <div class="panel-body">
                           <h4 class="text-center">Tiền Thanh Toán</h4>
                           <br>
                           <p class="text-center" style="color:red;font-size:20px"><?php echo number_format($tienthanhtoan) ?> đ</p>
                    
                        </div>
						<button style="margin:10px 200px;padding:5px 30px;font-size:20px" class="btn btn-danger" >Đặt Hàng</button>
                    </div>
                    <!--REVIEW ORDER END-->
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-pull-6 col-sm-pull-6">
                    <!--SHIPPING METHOD-->
                    <div class="panel panel-success">
                        <div class="panel-heading">Thông Tin</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <h4>Thông Tin Giao Hàng</h4>
                                </div>
                            </div>
                           
                           
                            <div class="form-group">
                                <div class="col-md-12"><strong>Họ Tên:</strong></div>
                                <div class="col-md-12">
                                    <input type="text" name="name" placeholder="Họ Và Tên"  class="form-control" value="<?php echo isset($user->name)? $user->name : '' ?>" />
                                    <p><b><?php echo form_error('name') ?> </b></p>
                                </div>
                            </div>
                           
                            <div class="form-group">
                                <div class="col-md-12"><strong>Email:</strong></div>
                                <div class="col-md-12"><input type="text" name="email" placeholder="Email" class="form-control" value="<?php echo isset($user->email)? $user->email : '' ?>" /></div>
                                <p><b><?php echo form_error('email') ?> </b></p>
                            </div>
                           
                            <div class="form-group">
                                <div class="col-md-12"><strong>Số Điện Thoại:</strong></div>
                                <div class="col-md-12"><input type="text" name="phone" placeholder="Số Điện Thoại"class="form-control" value="<?php echo isset($user->phone)? $user->phone : '' ?>" /></div>
                                <p><b><?php echo form_error('phone') ?> </b></p>
                            </div>
                            


                            <div class="form-group">
                                <div class="col-md-12"><strong>Hình Thức Thanh Toán:</strong></div>
                                
                                <div class="col-md-12">
                                    <select class="form-control" name="payment">
                                        <option value="">================Chọn Cổng Thanh Toán=============</option>
                                        <option value="tainha">Thanh Toán Khi Nhận Hàng</option>
                                        <option value="nganluong">Bảo Kim</option>
                                        <option value="nganluong">Ngân Lượng</option>
                                        
                                    
                                    </select>
                                    <p><b><?php echo form_error('payment') ?> </b></p>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-12"><strong>Ghi Chú:</strong></div>
                                <div class="col-md-12">
                                <p style="color:blue">(nhập địa chỉ và thời gian nhận hàng)</p>
                                <textarea class="form-control" rows="5" name="ghichu" value="<?php echo set_value('ghichu')?>"></textarea>
                                <p><b><?php echo form_error('ghichu') ?> </b></p>
              
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <!--SHIPPING METHOD END-->
                  
                </div>
                
                </form>
            </div>
            <div class="row cart-footer">
        
            </div>
    </div>