   <!-- Hero Section Begin -->
   
         <div class="container-fluid">
         <div class="row">
         <div class="col-lg-3">
            <div class="dropdown">
               <div id="dLabel" data-toggle="dropdown" class="btn btn-primary" data-target="#" href="#">
                  <i class="fas fa-align-justify"></i>
                  <span>Danh Mục Sản Phẩm</span>
                  <i class="fas fa-angle-down"></i>
               </div>
               <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
               <?php foreach ($catelog as $key): ?>
                  <li class="dropdown-submenu">
                     <a tabindex="-1" href="<?php echo base_url('san_pham_controller/danhmuc/' .$key->id) ?>"><?php echo $key->name ?></a>
                     <?php if(!empty($key->subs)): ?>
                     <ul class="dropdown-menu">
                        <?php foreach ($key->subs as $danhmuccon): ?>
                        <li><a tabindex="-1" href="<?php echo base_url('san_pham_controller/danhmuc/' .$danhmuccon->id) ?>"><?php echo $danhmuccon->name ?></a></li>
                       
                        <?php endforeach; ?>
                        
                     </ul>
                     <?php endif; ?>
                  </li>
                  <?php endforeach; ?>
               </ul>
            </div>
         </div>
         <div class="col-lg-9">
            <div class="hero__search">
               <div class="hero__search__form">
                  <form action="<?php echo base_url('san_pham_controller/timkiem') ?>" method="get">
                     <input type="text" placeholder="Tìm Kiếm" name="timkiem" />
                     <button type="submit" class="site-btn">Tìm Kiếm</button>
                  </form>
               </div>
               <div class="hero__search__phone">
                  <div class="hero__search__phone__icon">
                     <a href="<?php echo base_url('cart') ?>"> <i class="fa fa-cart-plus"></i></a>
                  </div>
                  <div class="hero__search__phone__text">
                     <a href="<?php echo base_url('cart') ?>">
                        <h6>Giỏ Hàng</h6>
                     </a>
                     <a href="<?php echo base_url('cart') ?>">
                     <span>Số Lượng: <b><?php echo $tongso?></b></span>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      
      <!-- Hero Section End -->