

<!DOCTYPE html>
<html lang="zxx">
   <head>
      <?php $this->load->view('slide/head',$this->data); ?>
   </head>
   <body>
      <!-- Header Section Begin -->
      <header class="header">
       <?php $this->load->view('slide/header',$this->data);?>
      </header>
      <!-- Header Section End -->
      <!-- Hero Section Begin -->
      <section class="hero">
        <?php $this->load->view('slide/navbar',$this->data); ?>
      </section>
      <!-- Hero Section End -->
	  
	 <?php $this->load->view($temp); $this->data?>
      <!-- Footer Section Begin -->
      <footer class="footer spad">
         <?php $this->load->view('slide/footer') ?>
      </footer>
      <!-- Footer Section End -->
   </body>
</html>

