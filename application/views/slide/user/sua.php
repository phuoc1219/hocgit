
<br>
<p class="text-center text-danger" style="font-size:20px;"> <?php echo $thongbao ?></p>
<div class="panel panel-success">
  <div class="panel-heading text-center">Cập Nhật Thông Tin</div>
  <div class="panel-body">
  	 <form class="form-horizontal" action="" method="post">
  		 <div class="form-group">
            <label class="control-label col-sm-3 text-primary" for="email">Họ Và Tên: <span class="text-danger">*</span></label>
            <div class="col-sm-9">
              <input type="text" class="form-control"  placeholder="Họ Và Tên" name="name" value="<?php echo $thongtin_sua->name?>">
              <p><b><?php echo form_error('name') ?> </b></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-3 text-primary" for="email">Email: </label>
            <div class="col-sm-9">
              <p><?php echo $thongtin_sua->email ?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-3 text-primary" for="email">Số Điện Thoại: <span class="text-danger">*</span></label>
            <div class="col-sm-9">
              <input type="text" class="form-control"  placeholder="Số Điện Thoại" name="phone" value="<?php echo $thongtin_sua->phone?>">
              <p><b><?php echo form_error('phone') ?> </b></p>
            </div>
          </div>
          
           <div class="form-group">
            <label class="control-label col-sm-3 text-primary" for="email">Địa Chỉ: <span class="text-danger">*</span></label>
            <div class="col-sm-9">
             
               <textarea class="form-control" rows="5" name="address"><?php echo $thongtin_sua->address?></textarea>
              <p><b><?php echo form_error('address') ?> </b></p>
            </div>
          </div>
          
          <div class="form-group">
        
            <label class="control-label col-sm-3 text-primary" for="pwd">Mật Khẩu Mới:</label>
            <div class="col-sm-9">
            	
              <input type="password" class="form-control" id="pwd" placeholder="Mật Khẩu" name ="password" value="<?php echo set_value('password')?>">
              <p><b><?php echo form_error('password') ?> </b></p>
              <span class="text-danger">(Chú ý: chỉ nhập mật khẩu khi cần thay đổi)</span>
            </div>
          </div>
          
           <div class="form-group">
            <label class="control-label col-sm-3 text-primary" for="pwd">Nhập lại Mật Khẩu Mới: </label>
            <div class="col-sm-9">
              <input type="password" class="form-control" id="pwd" placeholder="Mật Khẩu" name ="text_password" value="<?php echo set_value('text_password')?>">
              <p><b><?php echo form_error('text_password') ?> </b></p>
            </div>
          </div>
          
          <div class="form-group">
            <div class="col-sm-offset-5 col-sm-10">
              <button type="submit" class="btn btn-success">Cập Nhật</button>
              <a href="<?php echo base_url() ?>" class="btn btn-danger">trang Chủ</a>
            </div>
          </div>
	</form> 

  	
  	
  
  
  </div>
</div>