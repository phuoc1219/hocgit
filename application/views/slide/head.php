
      <meta charset="UTF-8" />
      <meta name="description" content="Ogani Template" />
      <meta name="keywords" content="Ogani, unica, creative, html" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta http-equiv="X-UA-Compatible" content="ie=edge" />
      <title>Ogani | Template</title>
      <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />
      <!-- jQuery library -->
      <link rel="stylesheet" href="<?php echo base_url('public/') ?>css/style.css" type="text/css" />
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <!-- Latest compiled JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
      <!-- Google Font -->
      <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet" />
      <script src="https://kit.fontawesome.com/a076d05399.js"></script>


      <link rel="stylesheet" href="<?php echo base_url('public/') ?>css/owl.carousel.min.css">
      <link rel="stylesheet" href="<?php echo base_url('public/') ?>css/owl.theme.default.min.css">
      <script src="<?php echo base_url('public/') ?>js/owl.carousel.min.js"></script>
      <!-- Css Styles -->
      