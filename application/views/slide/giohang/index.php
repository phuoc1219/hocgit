<div class="container-fluid">
        <h4 style="color:green">Giỏ Hàng Của Bạn</h4>
        <p>Tổng Sản Phẩm: <b style="color:red"><?php echo $total ?></b>
        <hr>
        <?php if($total>0): ?>
            <form action="<?php echo base_url() ?>cart/capnhat_cart" method='post'>
               <table id="cart" class="table table-hover table-condensed" style="background:white">
                  <thead>
                     <tr>
                        <th style="width:50%">Sản Phẩm</th>
                        <th style="width:10%">Giá</th>
                        <th style="width:8%">Số Lượng</th>
                        <th style="width:22%" class="text-center">Thành Tiền</th>
                        <th style="width:10%">Gỡ bỏ</th>
                     </tr>
                  </thead>
                  <?php $tienthanhtoan=0; ?>
                    <?php foreach ($cart as $key): ?>
                    <?php $kq=100 - $key['discount']; $giacu= $key['price']; $giamoi= $giacu*($kq/100);?>
                        <?php  $thanhtien= $giamoi*$key['qty'] ?>
                        <?php $tienthanhtoan= $tienthanhtoan + $thanhtien ?>
                  <tbody>
                  
                     <tr>
                        <td data-th="Product">
                           <div class="row">
                              <div class="col-sm-3 hidden-xs"><img style="width:100px;height:100px" src="<?php echo base_url('upload/san_pham/' .$key['img_link']) ?>" class="img-responsive"/></div>
                              <div class="col-sm-9">
                                 <h5 class="nomargin"><?php echo $key['name'] ?></h5>
                              </div>
                           </div>
                        </td>
                        <td data-th="Price"><?php echo number_format($giamoi) ?> đ</td>
                        <td data-th="Quantity">
                           <input type="number" name="sl_<?php echo $key['id'] ?>" class="form-control text-center" value="<?php echo $key['qty'] ?>">
                        </td>
                        <td data-th="Subtotal" class="text-center"><?php echo number_format($thanhtien) ?> đ</td>
                        <td class="actions" data-th="">
                           <a href="<?php echo base_url('cart/xoa/' .$key['id']) ?>" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i>
                           </a>								
                        </td>
                     </tr>
                  </tbody>
                  <?php endforeach; ?>
               </table>
               <div class="tfoot" style="float:right">
                  <div>
                     <h5 style="color:black;font-size:25px;margin:10px">Tiền Thanh Toán: <span style="color:red"><?php echo number_format($tienthanhtoan) ?> đ</span</h5>
                  </div>
                 
                  <div style="border-top:1px solid black;padding:10px;text-align:center;">
                     <input  class="btn btn-success" type="submit" value="Cập Nhật">
                     <a href="<?php echo base_url('cart/xoa/');?>" class="btn btn-primary ">Xóa Tất Cả </a>
                     <a href="<?php echo base_url('order/mua_sp') ?>" class="btn btn-danger" >Đặt Hàng <i class="fa fa-angle-right"></i></a>
                  </div>
               </div>
            </form>
            <?php else: ?>
                    <?php echo 'không có sản phẩm nào trong giỏ hàng' ?>
            <?php endif;?>
            </div>
         </div>