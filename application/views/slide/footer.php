  <!-- Footer Section Begin -->
 
         <div class="container">
            <div class="row">
               <div class="col-lg-3 col-md-6 col-sm-6">
                  <div class="footer__about">
                     <div class="footer__about__logo">
                     <?php foreach($list2 as $key3):?>
                        <a href="#"><img src="<?php echo base_url('upload/logo/'.$key3->image_link) ?>" alt="" /></a>
                        <?php endforeach; ?>
                     </div>
                     <ul>
                     <?php foreach($list1 as $key1):?>
                        <li><b>Địa Chỉ</b>:  <?php echo $key1->address ?></li>
                        <li><b>Số Điện Thoại:</b>  <?php echo $key1->phone ?></li>
                        <li><b>Email:</b>  <?php echo $key1->email ?></li>
                        <?php endforeach; ?>
                     </ul>
                  </div>
               </div>
               <div class="col-lg-3 col-md-5 col-sm-5 offset-lg-1">
                  <div class="footer__widget">
                     <h6>Nội Dung</h6>
                     <ul>
                        <li><a href="<?php echo base_url() ?>">Trang Chủ</a></li>
                        <li><a href="<?php echo base_url('gioithieu') ?>">Giới Thiệu</a></li>
                        <li><a href="<?php echo base_url('news') ?>">Tin Tức</a></li>
                        <li><a href="<?php echo base_url('cart') ?>">Giỏ Hàng</a></li>
                        
                     </ul>
                     
                  </div>
               </div>
               <div class="col-lg-5 col-md-12">
                  <div class="footer__widget">
                   <h6>Địa Chỉ Trên Bản Đồ</h6>
                     <div class="map">
                     <?php foreach($list1 as $key4):?>
						      <iframe src="<?php echo $key4->map ?>" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                     <?php endforeach; ?>
                     </div>
                     
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-12">
                  <div class="footer__copyright"></div>
               </div>
            </div>
         </div>
     
      <!-- Footer Section End -->