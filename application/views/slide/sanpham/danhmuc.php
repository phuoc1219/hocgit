<div class="container-fluid" style="background:white">
         <div class="col-sm-2 nav-sp">
            
            <div class="loc">
               <h5>lọc sản phẩm</h5>
               <form action="<?php echo base_url('san_pham_controller/tim_kiem_gia') ?>" method="get">
                  <div class="form-group">
                     <label for="email">Giá Từ:</label>
                    
                     <select class="form-control" id="sel1" name="giatu">
                     <?php for($i=0 ;$i <=1000000;$i=$i+10000): ?>
                        <option><?php echo $i ?> đ </option>
                     
                        <?php endfor; ?>
                     </select>
                   
                  </div>
                  <div class="form-group">
                     <label for="pwd">Đến Giá:</label>
                     <select class="form-control" id="sel1" name="dengia">
                     <?php for($a=0 ;$a <=1000000;$a=$a+10000): ?>
                        <option><?php echo $a ?> đ</option>
                      
                    <?php endfor; ?>
                     </select>
                  </div>
                  <button type="submit" class="btn btn-primary">Lọc</button>
               </form>
            </div>
            <div class="gird-left">
               <!-- Indicators -->
               <div class="title-nav">
                  <h5>Tin Tức Mới</h5>
               </div>

               <div class="left-news">
                  <?php foreach($list_news as $key): ?>
                  <div class="item-news">  
                     <img  src="<?php echo base_url('upload/tin_tuc/' .$key->image_link) ?>" alt="Los Angeles" >
                     <a href="<?php echo base_url('news/chitiet/'.$key->id) ?>"><?php echo $key->title ?></a>
                     <div class="clear"></div>
                  </div>
                  <hr>
                  <?php endforeach; ?>
                
                  
               </div>  
               
            </div>
            
         </div>
         
         <div class="col-sm-10" >
            
            <div class="flashsale-header-pro" style="margin-top:35px">
               <a href="flashsale-pro">
               <span class="flashsale-header-title-pro"><?php echo $danh_muc->name ?>    (<?php echo $total_sp ?>)</span>
               </a>
            </div>
            <div class="sp_pro">
            <?php foreach ($list as $key): ?>   
               <div class="col-md-3 col-sm-6 sp_dis" style="margin-bottom:20px;margin-top:10px;">
                     <div class="product-grid2">
                        <div class="product-image2">
                           <a href="#">
                                 <img class="pic-1" style="height:250px;" src="<?php echo base_url('upload/san_pham/' .$key->image_link) ?>">

                           </a>
                           <ul class="social">
                                 <li><a href="<?php  echo base_url('san_pham_controller/detail/'.$key->id)?>" data-tip="Chi Tiết"><i class="fa fa-eye"></i></a></li>
                                 <li><a href="<?php echo base_url('cart/them/'.$key->id) ?>" data-tip="Thêm Vào Giỏ"><i class="fa fa-shopping-cart"></i></a></li>
                           </ul>
                           <?php if($key->discount >0):?>
                           <span style="position:relative;z-index:10;top:-250px;background:green;color:white;padding:5px;" >-<?php  echo $key->discount ?>%</span>
                           <?php else: ?>
                              <span style="position:relative;z-index:10;top:-250px;background:green;color:white;padding:5px;opacity: 0;" ></span>
                           <?php endif; ?>
                        </div>
                        <div class="product-content">
                           <h3 class="title" style="height:60px; overflow: hidden;" ><a href="<?php  echo base_url('san_pham_controller/detail/'.$key->id)?>"><?php  echo $key->name ?></a></h3>
                           <?php if($key->discount>0): ?>
        			            <?php $kq=100 - $key->discount; $giacu=$key->price; $giamoi= $giacu*($kq/100);?>
                           <span class="price" style="font-size:16px"><b style="background:green;color:white"><?php   echo number_format($giamoi)?>đ</b>-<b style="text-decoration: line-through;"><?php   echo number_format($giacu)?>đ</b></span>
                           <?php else: ?>
                           <span class="price" style="font-size:16px"><b style="background:green;color:white"><?php   echo number_format($giamoi)?>đ</b></span>
                           <?php endif; ?>
                        </div>
                     </div>
               </div>
               
               <?php endforeach; ?> 

              
       
            </div>
            
         </div>
         <div class="clear"></div>            
            <div class="" style="height:50px;text-align:center">
               <?php echo $this->pagination->create_links();?>
          </div>
          <div class="clear"></div>
            
    
      </div>
     