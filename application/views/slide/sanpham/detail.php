<div class="container-fluid">
            <div class="detail">
               <div class="col-sm-9">
                  <div class="col-sm-5 img_minhoa">
                     <img src="<?php echo base_url('upload/san_pham/' .$product->image_link) ?>"  alt="Cinque Terre"> 
                  </div>
                  <div class="col-sm-7">
                     <h4><?php echo $product->name ?></h4>
                     <div >
                        <label class="label_detail" >Mã sản phẩm:</label>
                        <span class="item"><?php echo $product->id ?></span>
                     </div>
                     <div >
                        <label class="label_detail" >Danh Mục:</label>
                        <span class="item"><?php echo $catalog_sp->name ?></span>
                     </div>
                     <?php if($product->discount>0): ?>
                     <?php $kq=100 - $product->discount; $giacu=$product->price; $giamoi= $giacu*($kq/100);?>
                     <div >
                        <label class="label_detail" id="price"></label>
                        <span class="item_price"><?php echo number_format($giamoi)  ?>đ</span>
                     </div>
                     <div >
                        <label class="label_detail" >Giá Cũ: </label>
                        
                        <span class="item" id="discount"><?php echo number_format($giacu)  ?> đ</span>
                     </div>
                     <?php else: ?>
                        <div >

                        <label class="label_detail" id="price"></label>
                        <?php $kq=100 - $product->discount; $giacu=$product->price; $giamoi= $giacu*($kq/100);?>
                        <span class="item_price"><?php echo number_format($giamoi)  ?> đ</span>
                     </div>
                     <?php endif; ?>
                     <div >
                        <label class="label_detail" id="sku_142134">Lượt Xem:</label>
                        <span class="item"><?php echo $product->view ?></span>
                     </div>
                     <br>
                     <div >
                        <a href="<?php echo base_url('cart/them/'.$product->id) ?>" class="add_cart">Thêm Vào Giỏ</a>
                     </div>
                  </div>
                  <div class="form-group col-sm-12">
                     <label class="label_mota">Mô Tả Chi Tiết</label>
                     <div class="content_mota">
                     <?php echo $product->content ?>
                     </div>
                  </div>
               </div>
               <div class="col-sm-3 doitra" >
                  <div class="chinhsach">
                     <h5>Chính Sách Mua Hàng</h5>
                  </div>
                  <div class="chinhsach">
                     <i class="fas fa-phone-volume"></i>
                     <strong>Hỗ trợ mua nhanh</strong> <br><b style="color:red">19008198</b>
                  </div>
                  <div class="chinhsach">
                     <i class="far fa-hand-point-right"></i><strong>Hoàn tiền 100%</strong> Nếu phát hiện hàng giả
                  </div>
                  <div class="chinhsach">
                     <i class="fas fa-sync-alt"></i> <strong>Đổi trả trong 7 ngày</strong> Không chấp nhận trường hợp thay đổi suy nghĩ
                  </div>
                  <i class="fas fa-shipping-fast"></i><strong>Miễn phí vận chuyển</strong> Với đơn hàng trên 200k
               </div>
            </div>
         </div>










         