
         <div class="header__top">
            <div class="container">
               <div class="row">
                  <div class="col-lg-6 col-md-6">
                     <div class="header__top__left">
                        <ul>
                        <?php foreach($list1 as $key1):?>
                           <li><i class="fa fa-envelope"></i> <?php echo $key1->email ?></li>
                           <li><i class="fa fa-phone-alt"></i> <?php echo $key1->phone ?></li>
                           <?php endforeach; ?>
                        </ul>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6">
                     <div class="header__top__right">
                     <?php if(isset($user_info)): ?>
					         <div class="header__top__right__auth">
                           <a href="<?php echo base_url('user') ?>" ><i class="fa fa-user"></i>  <?php echo $user_info->name?></a>
                        </div>
                        <div class="header__top__right__auth">
                           <a href="<?php echo base_url('user/logout') ?>" ><i class="fa fa-sign-out-alt"></i> Đăng xuất</a>
                        </div>
                        <?php else: ?>
                        <div class="header__top__right__auth">
                           <a href="<?php echo base_url('user/dangky') ?>"><i class="fa fa-address-card"></i>Đăng Ký</a>
                        </div>
                        <div class="header__top__right__auth">
                           <a href="<?php echo base_url('user/dangnhap') ?>" ><i class="fa fa-sign-in-alt"></i> Đăng Nhập</a>
                        </div>
						      <?php endif;?>
						
                     
                    
                  </div>
               </div>
            </div>
         </div>
         </div>
         <div class="container" >
            <div class="row" >
               <div class="col-lg-3">
                  <div class="header__logo">
                     <a href="<?php echo base_url() ?>"><img style="width:200px;height:60px;" src="<?php echo base_url('upload/logo/')?>logo3.jpg" alt="" /></a>
                  </div>
               </div>
               <div class="col-lg-6">
                  <nav class="header__menu">
                     <ul>
                        <li class="active"><a href="http://localhost/van-phong-pham/"><i style="font-size:18px;" class="fas fa-home"></i> Trang Chủ</a></li>
                        <li><a href="<?php echo base_url('gioithieu/index') ?>">Giới Thiệu</a></li>
                        <li><a href="<?php echo base_url('news/index') ?>">Tin Tức</a></li>
                       
                     </ul>
                  </nav>
               </div>
            </div>
            <div class="humberger__open">
               <i class="fa fa-bars"></i>
            </div>
         </div>
     
      <!-- Header Section End -->