<!-- navbar side -->
<nav class="navbar-default navbar-static-side" role="navigation">
    <!-- sidebar-collapse -->
    <div class="sidebar-collapse">
        <!-- side-menu -->
        <ul class="nav" id="side-menu">
        
        
        
        		
   
            <br>
           
            <li>
                <a href="#"> Quản Lý Bán Hàng<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level" style="background:gray;">
                    <li>
                        <a href="<?php echo base_url('admin/transaction/index') ?>">Giao Dịch</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('admin/order/index') ?>">Đơn Hàng</a>
                    </li>
                </ul>
                <!-- second-level-items -->
            </li>
            <li>
                <a href="#"> Sản Phẩm<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level" style="background:gray;">
                    <li>
                        <a href="<?php echo base_url('admin/San_Pham/index') ?>">Sản Phẩm</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('admin/Danh_Muc') ?>">Danh Mục</a>
                    </li>
                   
                </ul>
                <!-- second-level-items -->
            </li>
            <li>
                <a href="#"> Tài Khoản<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level" style="background:gray;">
                    <li>
                        <a href="<?php echo  base_url()?>admin/admin/">Ban Quản Trị </a>
                    </li>
                   
                </ul>
                <!-- second-level-items -->
            </li>
            <li>
                <a href="#"> Hỗ Trợ & Liên Hệ<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level" style="background:gray;">
                   
                    <li>
                        <a href="<?php echo base_url('admin/Lien_He/index') ?>">Liên Hệ</a>
                    </li>

                </ul>
                <!-- second-level-items -->
            </li>
            <li>
                <a href="#"> Nội Dung<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level" style="background:gray;">
                    <li>
                        <a href="<?php echo  base_url()?>admin/logo/">Logo</a>
                    </li>
                    <li>
                        <a href="<?php echo  base_url()?>admin/Hinh_chay/">slide </a>
                    </li>
                    <li>
                        <a href="<?php echo  base_url()?>admin/news/">Tin Tức</a>
                    </li>
                    

                </ul>
                <!-- second-level-items -->
            </li>

        </ul>
        <!-- end side-menu -->
    </div>
    <!-- end sidebar-collapse -->
</nav>
<!-- end navbar side -->