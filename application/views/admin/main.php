<html>

<head>
    <?php $this->load->view('admin/head'); ?>
</head>

<body>
    <div id="wrapper">
        <?php $this->load->view('admin/header'); ?>
            <?php $this->load->view('admin/left'); ?>
    </div>

    <div id="page-wrapper">
        <?php $this->load->view($temp);?>

    </div>

    <!-- Core Scripts - Include with every page -->
    <script src="<?php echo base_url() ?>public/admin/assets/plugins/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url() ?>public/admin/assets/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>public/admin/assets/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url() ?>public/admin/assets/plugins/pace/pace.js"></script>
    <script src="<?php echo base_url() ?>public/admin/assets/scripts/siminta.js"></script>
    <!-- Page-Level Plugin Scripts-->
</body>

</html>