<div class="title">
	<h3>Bảng Sản Phẩm</h3>
	<p>Trang Quản Trị Sản Phẩm</p>

	<a href="<?php echo base_url() ?>admin/San_Pham/them"
		class="btn btn-primary" role="button"><span
		class="glyphicon glyphicon-align-left" style="color: #ff004c;"></span>
		Thêm Mới</a> <a href="<?php echo base_url() ?>admin/San_Pham/index"
		class="btn btn-primary" role="button"><span
		class="glyphicon glyphicon-align-left" style="color: #ff004c;"></span>
		Danh Sách</a>
</div>

 <b style="color:red;margin:10px 350px"><?php echo $thongbao; ?></b>
<!--bảng giao dịch-->
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-success">
			<div class="panel-heading">Danh Sách Sản Phẩm</div>
			<div class="panel-body">
				<b><p>Tổng Số Sản Phẩm:</b> <b style="color: red;"> <?php echo $tongso ?> </b>
				

				<table class="table table-bordered">
					<thead>
						<tr>
							
							<th>Mã Số</th>
							<th>Tên</th>

							<th>Giá</th>
							<th>Ngày Tạo</th>

							<th>Hành Động</th>


						</tr>
					</thead>
					<tbody>
					<?php foreach ($list as $key): ?>
						<tr>
							
							<td><?php echo $key->id ?></td>
							<td><img
								style="width: 100px; height: 70px; float: left; margin: 5px 5px;"
								alt=""
								src="<?php echo base_url('upload/san_pham/'.$key->image_link) ?>">
								<p>
									<b style="color: green"><?php echo $key->name ?></b>
								</p>

								
								</p>
								<p style="color: red; font-size: 12px">
									Lượt Xem:<b style="color: blue; font-size: 12px"><?php echo $key->view ?></b>
								</p></td>
								
									<td>
    								<?php if($key->discount>0): ?>
        								<?php $kq=100 - $key->discount; $giacu=$key->price; $giamoi= $giacu*($kq/100);?>
        								<b style="color:green;"><?php echo number_format($giamoi) ?> đ</b> 
        								<br>
        								<strike><?php echo number_format($key->price)?> đ</strike>
    								<?php else: ?>
    									<?php echo number_format($key->price)?> đ
    								<?php endif; ?>
									</td>
							
								
							<td><?php echo $key->created ?></td>



							<td>
								<a style="margin: 10px"
								href="<?php echo base_url() ?>admin/San_pham/xoa/<?php echo $key->id; ?>" title="Xóa"><i
									class=" glyphicon glyphicon-trash " style="font-size: 25px;"></i></a>
								
								<a style="margin: 10px"
								href="<?php echo base_url() ?>admin/San_pham/sua/<?php echo $key->id; ?>"
								title="Chỉnh Sữa"><i class=" glyphicon glyphicon-wrench "
									style="font-size: 25px;"></i></a> 
								<a style="margin: 10px" href="<?php echo base_url() ?>admin/San_pham/chitiet/<?php echo $key->id; ?>" title="Chi Tiết" > <i
									class="glyphicon glyphicon-th-large" style="font-size: 25px;"></i></a>
							</td>
						</tr>
					</tbody>
					<?php endforeach; ?>
					
				</table>
			</div>
		</div>
		
        
         
  
 			
 			
  <?php echo $this->pagination->create_links();?>
		 
	</div>

</div>



