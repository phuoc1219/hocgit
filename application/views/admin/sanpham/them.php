

<html>


<body>
<div class="title">
	<h3>Thêm Sản Phẩm</h3>
	<p>Trang Quản Trị Sản Phẩm</p>

	<a href="<?php echo base_url() ?>admin/San_pham/index" class="btn btn-primary" role="button"><span
		class="glyphicon glyphicon-align-left" style="color: #ff004c;"></span>
		Danh Sách</a>

</div>

<div class="panel panel-success" style="margin:10px 30px;border-radius:50px;">
  <div class="panel-heading text-center"><b>Thông Tin</b></div>
  <div class="panel-body">
  	<form action="" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<div class="form-group">
								<label for="id" style="color:blue;">Tên Sản Phẩm:</label>
								 <input type="text" class="form-control" placeholder="Tên Sản Phẩm" name="name" value="<?php echo set_value('name')?>">
									<p><b><?php echo form_error('name') ?> </b></p>
									
							</div>


					
						
						<div>
							<label  style="color:blue;">Danh Mục:</label>
							
                              <select class="form-control" name="catalog" >
                              <option value=""></option>
                              <?php foreach ($danhmuc as $key): ?>
                                 	<?php if($key->subs >1): ?>
                                    	<option disabled="disabled" style="color:red; font-weight:bold"><?php echo $key->name ?>:</option>
                                    	
                                    	<?php foreach ($key->subs as $danhmuccon):  ?>
                                    	<option style="color:black;" value="<?php echo $danhmuccon->id ?>">&ensp; <?php echo $danhmuccon->name ?></option>
                                    	<?php endforeach; ?>
                                    <?php else: ?>
                                    		<option value="<?php echo $key->id?>" ><?php echo $key->name?></option>
                                    <?php endif; ?>
                                     
                              <?php endforeach; ?>
                              </select>
                              
								
						</div>

						<div class="form-group">
							<label for="id" style="color:blue;">Giá (VNĐ):</label> 
							<input type="text" class="form-control" placeholder="Giá Sản Phẩm" name="price" value="<?php echo set_value('price')?>"> 
								<p><b><?php echo form_error('price') ?> </b></p>
						</div>
						<div class="form-group">
							<label for="id" style="color:blue;">Giảm Giá (%):</label> 
							<input type="text" class="form-control" placeholder="Giảm Giá" name="discount" value="<?php echo set_value('discount')?>">
								
						</div>

						<div class="form-group">
							<label  style="color:blue;">Hình Sản Phẩm:</label> <input type="file"
								class="form-control"  name="img_link" >
								<p><b> </b></p>
						</div>

						
						
						<div class="form-group">
							<label  style="color:blue;">Mô Tả:</label> 
							<textarea name="content" class="form-control" placeholder="Số Lượng Sản Phẩm" rows="5" value="<?php echo set_value('content')?>"></textarea>
								<p><b> </b></p>
						</div>
						<div class="form-group">
							<label for="pwd" style="color:blue;">Số Lượng:</label> 
							<input type="text" class="form-control"  name="total" value="">
								<p><b> </b></p>
						</div>
						
						
						<div class="form-group">
							<label for="pwd" style="color:blue;">Ngày Tạo:</label> 
							<input type="date" class="form-control"  name="ngaytao" value="">
								<p><b> </b></p>
						</div>
						
						
                                     
        							
						<button  type="submit" style="margin: 10px 400px;"
							class="btn btn-primary">Thêm Mới</button>
					</form>
  	
  
  
  </div>
</div>
					


</div>
</body>
</html>