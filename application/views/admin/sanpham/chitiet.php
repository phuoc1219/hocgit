<div class="title">

    <a href="<?php echo base_url() ?>admin/San_Pham/index" class="btn btn-primary" role="button"><span
		class="glyphicon glyphicon-align-left" style="color: #ff004c;"></span>
		Danh Sách</a>
</div>

<b style="color: red; margin: 10px 350px"></b>
<!--bảng giao dịch-->
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-success">
            <div class="panel-heading">Chi Tiết Sản Phẩm</div>
            <div class="panel-body">

                <div class="row" style="float: left;">
                    <div class="col-sm-7">
                        <img src="<?php echo base_url('upload/san_pham/'.$chitiet->image_link) ?>" style="width:280px;height:300px;margin: 10px;">

                    </div>

                    <div class="col-sm-5">

                        <table class="table">

                            <tbody>
                                <tr>
                                    <td class="tieude" style="font-weight: bold;">Mã Số:</td>
                                    <td>
                                        <?php echo $chitiet->id ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tieude" style="font-weight: bold;">Tên:</td>
                                    <td style="font-weight: bold;color:red">
                                        <?php echo $chitiet->name ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tieude">Giá:</td>

                                    <td style="font-weight: bold;color:red">
                                        <?php  $gia = $chitiet->price; echo number_format($gia);?> VNĐ</td>
                                </tr>
                                <tr>
                                    <td class="tieude">Giá Giảm:</td>

                                    <td style="font-weight: bold;color:red">
                                        <?php  $giagiam = $chitiet->discount; echo number_format($giagiam); ?> VNĐ</td>
                                </tr>
                                <tr>
                                    <td class="tieude">Lượt Xem:</td>

                                    <td style="font-weight: bold;">
                                        <?php echo $chitiet->view ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tieude">Đã Mua:</td>

                                    <td style="font-weight: bold;">
                                        <?php echo $chitiet->buyed ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tieude">Số Lượng:</td>

                                    <td style="font-weight: bold;">
                                        <?php echo $chitiet->total ?>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
            <div>
                <b style="margin:10px;color:blue;">Mô Tả Chi tiết:</b>
                <div style="border:1px solid black;width:550px;height:200px;overflow:scroll;margin:10px;padding:5px">

                    <?php echo $chitiet->content ?>

                </div>

            </div>

        </div>
    </div>

</div>

</div>