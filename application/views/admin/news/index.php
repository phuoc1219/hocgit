<div class="title">
	<h3>Bảng Tin Tức</h3>
	<p>Trang Quản Trị Nội Dung</p>

	<a href="<?php echo base_url() ?>admin/news/them"
		class="btn btn-primary" role="button"><span
		class="glyphicon glyphicon-align-left" style="color: #ff004c;"></span>
		Thêm Mới</a> <a href="<?php echo base_url() ?>admin/news/index"
		class="btn btn-primary" role="button"><span
		class="glyphicon glyphicon-align-left" style="color: #ff004c;"></span>
		Danh Sách</a>
</div>

 <b style="color:red;margin:10px 350px"><?php echo $thongbao; ?></b>
<!--bảng giao dịch-->
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-success">
			<div class="panel-heading">Danh Sách Tin Tức</div>
			<div class="panel-body">
				<b><p>Tổng Số Tin:</b> <b style="color: red;"> <?php echo $tongso ?> </b>
				

				<table class="table table-bordered">
					<thead>
						<tr>
							
							<th>Mã Số</th>
                            <th>Hình</th>
							<th>Tiêu Đề</th>

							<th>Tóm Tắt</th>
							

							<th>Ngày Tạo</th>
                            <th>Hành Động</th>


						</tr>
					</thead>
					<tbody>
					<?php foreach ($list as $key): ?>
						<tr>
							
							<td>
                                <?php echo $key->id ?>
                            </td>

							<td>
                                <img
								style="width: 100px; height: 100px; float: left; margin: 5px 5px;"
								alt=""
								src="<?php echo base_url('upload/tin_tuc/'.$key->image_link) ?>">
								
                            </td>
                            <td>
    							<?php echo $key->title ?>
							</td>
                            <td>
                                <?php echo $key->summary ?>
							</td>
								
							
								
							<td>
                                <?php echo $key->created ?>
                            </td>



							<td>
								<a style="margin: 10px"
								href="<?php echo base_url() ?>admin/news/xoa/<?php echo $key->id; ?>" title="Xóa"><i
									class=" glyphicon glyphicon-trash " style="font-size: 25px;"></i></a>
								
								<a style="margin: 10px"
								href="<?php echo base_url() ?>admin/news/sua/<?php echo $key->id; ?>"
								title="Chỉnh Sữa"><i class=" glyphicon glyphicon-wrench "
									style="font-size: 25px;"></i></a> 
								<a style="margin: 10px" href="<?php echo base_url() ?>admin/news/chitiet/<?php echo $key->id; ?>" title="Chi Tiết" > <i
									class="glyphicon glyphicon-th-large" style="font-size: 25px;"></i></a>
							</td>
						</tr>
					</tbody>
					<?php endforeach; ?>
					
				</table>
			</div>
		</div>
		
        
         
  
 			
 			
  <?php echo $this->pagination->create_links();?>
		 
	</div>

</div>



