

<html>


<body>
<div class="title">
	<h3>Thêm Tin Tức</h3>
	<p>Trang Quản Trị Nội Dung</p>

	<a href="<?php echo base_url() ?>admin/news/index" class="btn btn-primary" role="button"><span
		class="glyphicon glyphicon-align-left" style="color: #ff004c;"></span>
		Danh Sách</a>

</div>

<div class="panel panel-success" style="margin:10px 30px;border-radius:50px;">
  <div class="panel-heading text-center"><b>Thông Tin</b></div>
  <div class="panel-body">
  	<form action="" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<div class="form-group">
								<label for="id" style="color:blue;">Tiêu Đề:</label>
								 <input type="text" class="form-control" placeholder="Tên Sản Phẩm" name="title" value="<?php echo set_value('title')?>">
									<p><b><?php echo form_error('title') ?> </b></p>
									
							</div>

						<div>
                        <div class="form-group">
							<label  style="color:blue;">Tóm Tắt:</label> 
							<textarea name="summary" class="form-control" placeholder="Tóm Tắt Nội Dung" rows="5" value="<?php echo set_value('summary')?>"></textarea>
								<p><b> </b></p>
						</div>
                        <div class="form-group">
							<label  style="color:blue;">Nội Dung :</label> 
							<textarea name="content" class="form-control" placeholder="Nội Dung" rows="20" value="<?php echo set_value('content')?>"></textarea>
                            <p><b><?php echo form_error('content') ?> </b></p>
						</div>

						
						<div class="form-group">
							<label  style="color:blue;">Hình Sản Phẩm:</label> <input type="file"
								class="form-control"  name="img_link" >
								<p><b><?php echo form_error('img_link') ?> </b></p>
						</div>

						

						
						<div class="form-group">
							<label style="color:blue;">Ngày Tạo:</label> 
							<input type="date" class="form-control"  name="ngaytao" value="">
                            <p><b><?php echo form_error('ngaytao') ?> </b></p>
						</div>
						
						
                                     
        							
						<button  type="submit" style="margin: 10px 400px;"
							class="btn btn-primary">Thêm Mới</button>
					</form>
  	
  
  
  </div>
</div>
					


</div>
</body>
</html>