<div class="title">

    <a href="<?php echo base_url() ?>admin/San_Pham/index" class="btn btn-primary" role="button"><span
		class="glyphicon glyphicon-align-left" style="color: #ff004c;"></span>
		Danh Sách</a>
</div>

<b style="color: red; margin: 10px 350px"></b>
<!--bảng giao dịch-->
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-success">
            <div class="panel-heading">Chi Tiết Tin Tức</div>
            <div class="panel-body">
                <div class="text-center">
                
                    <h3 style="color:green;font-weight:bold"> 
                        <?php echo $chitiet->title ?>
                    </h3>
                
                </div>
                <hr>
                 <div class="text-center">
                
                 <img src="<?php echo base_url('upload/tin_tuc/'.$chitiet->image_link) ?>" style="width:280px;height:300px;margin: 10px;">
                
                </div>
                
            </div>
            <div>
                
                <div style="margin:10px;padding:5px;font-size:17px;">

                    <?php echo $chitiet->content ?>

                </div>

            </div>

        </div>
    </div>

</div>

</div>