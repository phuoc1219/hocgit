<html>
    <head>
        <?php $this->load->view('admin/head'); ?>
        <style>
            body{
                background-image: url('<?php echo base_url() ?>public/admin/assets/img/bg-01.jpg') ;
                width: 100%;  
                min-height: 100vh;
                display: -webkit-box;
                display: -webkit-flex;
                display: -moz-box;
                display: -ms-flexbox;
                display: flex;
                flex-wrap: wrap;
                justify-content: center;
                align-items: center;
                padding: 15px;
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
                position: relative;
            }
            
        </style>
    </head>
    <body>
        <div class="login-form">
            <form method="post">

                <h2 class="text-center">ADMIN</h2>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input type="text" class="form-control" placeholder="Username" name="taikhoan">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input type="password" class="form-control" placeholder="Password" name="matkhau">
                    </div>
                </div>
                
                <div style="color:red;text-align:center"><b><?php echo form_error('login') ?></b></div>
                
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">Đăng Nhập</button>
                </div>

            </form>

        </div>


    </body>
</html>