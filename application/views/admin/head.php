<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>ADMIn</title>
        <!-- Core CSS - Include with every page -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="<?php echo base_url() ?>public/admin/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="<?php echo base_url() ?>public/admin/assets/plugins/pace/pace-theme-big-counter.css" rel="stylesheet" />
        <link href="<?php echo base_url() ?>public/admin/assets/css/style.css" rel="stylesheet" />
        <link href="<?php echo base_url() ?>public/admin/assets/css/main-style.css" rel="stylesheet" />
        <link rel="shortcut icon" type="image/png" href="<?php echo base_url() ?>public/admin/assets/img/favicon-32x32.png"/>
    </head>
  
</html>