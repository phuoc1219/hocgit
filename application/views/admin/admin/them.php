

<html>


<body>
<div class="title">
	<h3>Thêm Admin</h3>
	<p>Trang Quản Trị Tài Khoản</p>

	<a href="<?php echo base_url() ?>admin/admin/index" class="btn btn-primary" role="button"><span
		class="glyphicon glyphicon-align-left" style="color: #ff004c;"></span>
		Danh Sách</a>

</div>

<div class="panel panel-success" style="margin:10px 30px;border-radius:50px;">
  <div class="panel-heading text-center"><b>Thông Tin</b></div>
  <div class="panel-body">
  	<form action="" method="post">
						<div class="form-group">
							<div class="form-group">
								<label for="id" style="color:blue;">Họ Tên:</label> <input type="text"
									class="form-control" placeholder="Họ Tên" name="name" value="<?php echo set_value('name')?>">
									<p><b><?php echo form_error('name') ?> </b></p>
									
							</div>


						<div>
							<label for="email" style="color:blue;">Email:</label> <input type="email"
								class="form-control" placeholder="Địa Chỉ Email" name="email" value="<?php echo set_value('email')?>">
								<p><b><?php echo form_error('email') ?> </b></p>
						</div>

						<div class="form-group">
							<label for="id" style="color:blue;">Số Điện Thoại:</label> <input type="text"
								class="form-control" placeholder="Số Điện Thoại" name="phone" value="<?php echo set_value('phone')?>"> 
								<p><b><?php echo form_error('phone') ?> </b></p>
						</div>
						<div class="form-group">
							<label for="id" style="color:blue;">Địa Chỉ:</label> <input type="text"
								class="form-control" placeholder="Địa Chỉ" name="address" value="<?php echo set_value('address')?>">
								<p><b><?php echo form_error('address') ?> </b></p>
						</div>

						<div class="form-group">
							<label for="pwd" style="color:blue;">Mật Khẩu:</label> <input type="password"
								class="form-control" placeholder="Mật Khẩu" name="password" value="<?php echo set_value('password')?>">
								<p><b><?php echo form_error('password') ?> </b></p>
						</div>

						<div class="form-group">
							<label for="pwd" style="color:blue;">Nhập Lại Mật Khẩu:</label> <input type="password"
								class="form-control" placeholder="Mật Khẩu" name="text_password" value="<?php echo set_value('text_password')?>">
								<p><b><?php echo form_error('text_password') ?> </b></p>
						</div>


						<button  type="submit" style="margin: 10px 400px;"
							class="btn btn-primary">Thêm Mới</button>
					</form>
  	
  
  
  </div>
</div>
					


</div>
</body>
</html>