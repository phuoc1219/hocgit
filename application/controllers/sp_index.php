<?php


class sp_index extends MY_Controller
{

	 function sp_moi()
	{
		$this->load->library('pagination');
        $this->load->model('San_Pham_Model');
	   
        //lấy ID của thể loại
        $id = intval($this->uri->rsegment(3));
      
       $input=array();
        //lấy danh sách sản phẩm thuộc danh mục đó
        //         lấy tổng số danh sách
        $TongSo= $this->San_Pham_Model->get_tongso($input);
        $data['total_sp']=$TongSo;
        
        //load thư viện phân trang
        $this->load->library('pagination');
        
        $config=array();
        
        $config['total_rows']=  $TongSo; //tổng tất cả sản phẩm trong web
        $config['base_url']=base_url('sp_index/sp_moi/' .$id); //Đường dẫn của từng đoạn phân trang ( link của các nút phân trang )
        $config['per_page']    = 12;//Số lượng phần tử hiển thị trên một trang.
        $config['uri_segment'] = 4;//Xác định phân đoạn chứa số trang
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);
        
        
        $config['full_tag_open'] = "<ul class='pagination '>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#" >';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        
        
        
        $config['prev_link'] = '<i class="fa fa-angle-double-left"></i>Trước';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        
        
        $config['next_link'] = 'Sau<i class="fa fa-angle-double-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        
        //khỡi tạo phân trang
        $this->pagination->initialize($config);
       
        $star=intval($this->uri->segment(4));
        
        $input['limit']=array( $config['per_page'],$star );
        
        
		


		
	    $sanpham=$this->San_Pham_Model->get_Danh_Sach($input);
	    
		$data['list']=$sanpham;
		
		
		$data['temp']='slide/sp-index/sp_moi';
        $this->load->view('slide/layout',$data);
		
    }
    function sp_KM()
	{
        $this->load->model('San_Pham_Model');
	   
        
        $this->load->library('pagination');
       
        //lấy ID của thể loại
        $id = intval($this->uri->rsegment(3));
      
       $input=array();
        //lấy danh sách sản phẩm thuộc danh mục đó
        //         lấy tổng số danh sách
        $TongSo= $this->San_Pham_Model->get_tongso($input);
        $data['total_sp']=$TongSo;
        
        //load thư viện phân trang
        $this->load->library('pagination');
        
        $config=array();
        
        $config['total_rows']=  $TongSo; //tổng tất cả sản phẩm trong web
        $config['base_url']=base_url('sp_index/sp_KM/' .$id); //Đường dẫn của từng đoạn phân trang ( link của các nút phân trang )
        $config['per_page']    = 12;//Số lượng phần tử hiển thị trên một trang.
        $config['uri_segment'] = 4;//Xác định phân đoạn chứa số trang
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);
        
        
        $config['full_tag_open'] = "<ul class='pagination '>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#" >';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        
        
        
        $config['prev_link'] = '<i class="fa fa-angle-double-left"></i>Trước';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        
        
        $config['next_link'] = 'Sau<i class="fa fa-angle-double-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        
        //khỡi tạo phân trang
        $this->pagination->initialize($config);
       
        $star=intval($this->uri->segment(4));
        
        $input['limit']=array( $config['per_page'],$star );
        
        
		
	    //sản phẩm mới
	    
		//sản phẩm khuyến mãi
		$input['where']=array('discount >='=>'1');
		
	    $khuyenmai=$this->San_Pham_Model->get_Danh_Sach($input);
		$data['KM']=$khuyenmai;
		
		
		
		$data['temp']='slide/sp-index/sp_KM';
        $this->load->view('slide/layout',$data);
		
	}
	
	
}
