<?php
class cart extends  MY_Controller
{
    function __construct()
    {
       parent::__construct();
        $this->load->library('cart');
        $this->load->model('San_Pham_Model');
    }
    
    //hàm thêm sản phẩm vào giỏ
    function them()
    {
        
   
        $id=$this->uri->rsegment(3);
        $san_pham=$this->San_Pham_Model->get_Thong_tin($id);
        if(!$san_pham)
        {
            redirect();
        }
        $sl=1;
        
        
               $sl=1;
        $cart = array(
                
                        'id'    => $san_pham->id,
                        'qty'   => $sl,
                        'price' => $san_pham->price,
                        'discount'=>$san_pham->discount,
                        'name'  => $san_pham->name,
                        'img_link'=>$san_pham->image_link,
        );
                        
                        
                      
             
                
    
    
    
         $this->cart->insert($cart);
         redirect(base_url('cart'));
    }
    function index()
    {
        $cart=$this->cart->contents();
        $total=$this->cart->total_items();
      
        $data['cart']=$cart;
        $data['total']=$total;
        
        $data['temp']='slide/giohang/index';
        $this->load->view('slide/layout', $data);
       
    }
    
    function  capnhat_cart()
    {
       
            $cart=$this->cart->contents();
           
            foreach ($cart as $row=> $key)
            {
                //số lượng sp
                $sl=$this->input->post('sl_'.$key['id']);
                $data=array();
                $data['rowid']=$row;
                $data['qty']=$sl;
                $this->cart->update($data);
            }
            redirect(base_url('cart'));
        
    }
    
    //xoa sản phẩm giỏ hàng
    function xoa()
    {
        $id=$this->uri->rsegment(3);
        $id=intval($id);
        //xóa 1 sản phẩm trong giỏ hàng
        if($id >0)
        {
            $cart=$this->cart->contents();
            
            foreach ($cart as $row=> $key)
            {
                if($key['id'] == $id)
                {
                    //số lượng sp
                    $sl=$this->input->post('sl_'.$key['id']);
                    $data=array();
                    $data['rowid']=$row;
                    $data['qty']=0;
                    $this->cart->update($data);
                }
               
            }
            
        }
        else 
        {
            //xóa toàn bộ giỏ hàng
            $this->cart->destroy();
        }
        redirect(base_url('cart'));
    }
}