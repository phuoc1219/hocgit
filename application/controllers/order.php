<?php
class order extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('user_model');
    }
    function mua_sp()
    {
        $cart=$this->cart->contents();
        if(count($cart) <=0 )
        {
            redirect();
        }
        
       
        
        
        // lấy ra tổng số tiền cần thanh toán
        $tienthanhtoan=0;
        foreach ($cart as $key)
        {
            $kq=100 - $key['discount']; $giacu= $key['price']; $giamoi= $giacu*($kq/100);
            $thanhtien= $giamoi*$key['qty'];
            $tienthanhtoan= $tienthanhtoan + $thanhtien;
        }
        
        $data['tienthanhtoan']=$tienthanhtoan;
        
        
        
        $user_id=0;
        $user='';
        if($this->session->userdata('user_id_login'))
        {
            
            //lấy thông tin thành viên
            $user_id = $this->session->userdata('user_id_login');
            $user = $this->user_model->get_Thong_Tin($user_id);
            
        }
       
        
        $data['user']=$user;
        
        
        if($this->input->post())
        {
            
           
            
            $this->form_validation->set_rules('name','Họ Tên','required|min_length[8]');
            $this->form_validation->set_rules('email','email nhận hàng','required|min_length[8]|valid_email');
            $this->form_validation->set_rules('phone','số điện thoại','required');
            $this->form_validation->set_rules('payment','cổng thanh toán','required');
         
            $this->form_validation->set_rules('ghichu','ghi chú','required');
            
            
            
            if($this->form_validation->run())
            {
                $payment=$this->input->post("payment");
                $name=$this->input->post("name");
                $email=$this->input->post("email");
                $phone=$this->input->post("phone");
             
                $ghichu=$this->input->post("ghichu");
                
                $status=0;
                
                $data=array(
                        'status'=>$status,//trạng thái chưa thanh toán
                        'user_id'=>$user_id,//id người mua hàng nếu đã đang nhập
                        'user_name'=>$name,
                        'user_email'=>$email,
                        'user_phone'=>$phone,
                        'message'=>$ghichu,
                        'amount'=>$tienthanhtoan,
                        'payment'=>$payment,
                        'created'=>now(),
                       
                        
                        
                        
                );
                //thêm dữ liệu vào bảng transaction
                $this->load->model('transaction_model');
               
                $this->transaction_model->them($data);
               
                
                //thêm vào bảng order (chi tiết đơn hàng)
                $this->load->model('order_model');
                $transaction_id=$this->db->insert_id();//lấy ra id của giao dịch vừa thêm vào
                foreach ($cart as $key)
                {
                   
                    $data=array(
                            'transaction_id' =>$transaction_id,
                            'product_id'     =>$key['id'], 
                            'qty'            =>$key['qty'],
                            'amount'         =>$thanhtien,
                            'status'         =>0,

                    );
                    $this->order_model->them($data);
                   
                
                   
                }
                //xoa thông tin giỏ hàng
                //xóa toàn bộ giỏ hàng
                $this->session->set_flashdata('thongbao','Bạn Đã Đặt Hàng Thành Công!.');
                $this->cart->destroy();
                redirect(base_url('thanhcong'));
            
                //chuyển tới trang mua sp
               
            }
        }
        
        
        //lấy ra nội dung thông báo
        $thongbao=$this->session->flashdata('thongbao');
        $data['thongbao']=$thongbao;
        
        
        $data['temp']='slide/order/muahang';
        $this->load->view('slide/layout', $data);
    }
    
}