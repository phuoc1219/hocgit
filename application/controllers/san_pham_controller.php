<?php
class san_pham_controller extends MY_Controller
{
    function __construct()
    {
       
            //ke thua tu CI_Controller
            parent::__construct();
            //load model sản phẩm
            $this->load->model('San_Pham_Model');
            $this->load->model('Danh_Muc_Model');
            $this->load->model('news_Model');
            
        
    }
    
    
    //hiển thị danh sách sản phẩm theo danh mục
    function danhmuc()
    {
        //lấy ID của thể loại
        $id = intval($this->uri->rsegment(3));
        //lay ra thông tin của thể loại
       
        $dmsp = $this->Danh_Muc_Model->get_Thong_Tin($id);
        if(!$dmsp)
        {
            redirect();
        }
        
        $data['danh_muc'] = $dmsp;
        $input = array();
        $input['where'] = array('category_id' => $id);
       
        //lấy danh sách sản phẩm thuộc danh mục đó
        //         lấy tổng số danh sách
        $TongSo= $this->San_Pham_Model->get_tongso($input);
        $data['total_sp']=$TongSo;
        
        //load thư viện phân trang
        $this->load->library('pagination');
        
        $config=array();
        
        $config['total_rows']=  $TongSo; //tổng tất cả sản phẩm trong web
        $config['base_url']=base_url('san_pham_controller/danhmuc/' .$id); //Đường dẫn của từng đoạn phân trang ( link của các nút phân trang )
        $config['per_page']    = 8;//Số lượng phần tử hiển thị trên một trang.
        $config['uri_segment'] = 4;//Xác định phân đoạn chứa số trang
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);
        
        
        $config['full_tag_open'] = "<ul class='pagination '>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#" >';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        
        
        
        $config['prev_link'] = '<i class="fa fa-angle-double-left"></i>Trước';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        
        
        $config['next_link'] = 'Sau<i class="fa fa-angle-double-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        
        //khỡi tạo phân trang
        $this->pagination->initialize($config);
       
        $star=intval($this->uri->segment(4));
        
        $input['limit']=array( $config['per_page'],$star );
        
        
        
        
        
        
        //         lấy danh sách sản phẩm
       
      
        $DanhSach=$this->San_Pham_Model->get_Danh_Sach($input);
        
        $data['list']=$DanhSach;


       
        $DanhSach_news=$this->news_Model->get_Danh_Sach();
        
        $data['list_news']=$DanhSach_news;

        
        //hiển thị ra view
        $data['temp']='slide/sanpham/danhmuc';
        $this->load->view('slide/layout',$data);
        
      
      
    }

    
    
    function detail()
    {
        //lay id san pham muon xem
        $id = $this->uri->rsegment(3);
        $product = $this->San_Pham_Model->get_Thong_Tin($id);
        if(!$product) redirect();
        $this->data['product'] = $product;
        
        
        
        //lay thong tin cua danh mục san pham
        $catalog = $this->Danh_Muc_Model->get_Thong_Tin($product->category_id);
        $this->data['catalog_sp'] = $catalog;
        
        
        
        //cập nhật lại lượt xem khi ckick vào sản phẩm
        
        $data=array();
        $data['view']=$product->view + 1;
        $this->San_Pham_Model->sua($product->id,$data);
        
        
        
        //hiển thị ra view
        $this->data['temp'] = 'slide/sanpham/detail';
        $this->load->view('slide/layout', $this->data);
    }
    
    
    
    
    function timkiem()
    {
        $giatri=$this->input->get('timkiem');
        $this->data['giatri']=trim($giatri);
        $input['like']=array('name' ,$giatri);
        $list=$this->San_Pham_Model->get_Danh_sach($input);
        $this->data['list_sp']=$list;
       

       


       $this->data['temp'] = 'slide/sanpham/timkiem';
       $this->load->view('slide/layout', $this->data);
    }
    
    
    //tìm kiếm theo giá sản phẩm
    function tim_kiem_gia()
    {
        $giatu= intval( $this->input->get('giatu'));
        $dengia= intval( $this->input->get('dengia'));
        
        //lọc theo giá
        $input=array();
        
        $input['where']=array('price >='=> $giatu,'price <=' => $dengia);
        $list_gia=$this->San_Pham_Model->get_Danh_sach($input);
       $this->data['timkiem_gia']=$list_gia;
       $this->data['giatu']=$giatu;
       $this->data['dengia']=$dengia;
       
     
        $this->data['temp'] = 'slide/sanpham/timkiem_gia';
        $this->load->view('slide/layout', $this->data);
       
    }
}