<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_controller {

	public function index()
	{
		$data = array();
		$data['temp']='admin/order/index';
		$this->load->view('admin/main',$data);
	}
	
	
	//đang xuât khỏi trang quản trị
	function DangXuat()
	{
	    if($this->session->userdata('login'))
	    {
	        $this->session->unset_userdata('login');
	        
	    }
	    redirect(base_url('admin/login/index'));
	}
}
