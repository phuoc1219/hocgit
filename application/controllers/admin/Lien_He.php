<?php
class Lien_He extends My_controller
{
    function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('Lien_He_Model');
    }
    
    function index()
    {
        $input=array();
        //         lấy danh sách
        $DanhSach=$this->Lien_He_Model->get_Danh_Sach($input);
        
        $data['list']=$DanhSach;
      
        
        //lấy ra nội dung thông báo ỏ them()
        $thongbao=$this->session->flashdata('thongbao');
        $data['thongbao']=$thongbao;
        
        
        
        $data['temp']='admin/lienhe/index';
        $this->load->view('admin/main',$data);
    }
    function them()
    {
        if($this->input->post())
        {
            $this->form_validation->set_rules('phone','Số Điện Thoại','required');
            $this->form_validation->set_rules('address','địa chỉ','required');
            $this->form_validation->set_rules('email','email','required');
            
            
            
            
            if($this->form_validation->run())
            {
                
                $phone=$this->input->post("phone");
                $address=$this->input->post("address");
                $email=$this->input->post("email");
                $map=$this->input->post("map");
                
                $data=array(
                        'phone'=>$phone,
                        'address'=>$address,
                        'email'=>$email,
                        'map'=>$map
                        
                        
                        
                );
                
                if($this->Lien_He_Model->them($data))
                {
                    $this->session->set_flashdata('thongbao','Bạn Đã Thêm Thành Công!.');
                }
                else {
                    $this->session->set_flashdata('thongbao','Bạn Thêm Không Thành Công,Vui Lòng Kiểm Tra Lại.');
                }
                redirect(base_url('admin/Lien_He/index'));
                
            }
            
        }
        
        
        
        $data['temp']='admin/lienhe/them';
        $this->load->view('admin/main',$data);
    }
    function sua()
    {
        
        $id=$this->uri->segment(4);
        $id=intval($id);
        //lấy thông tin
        
        $input=array();
        //         lấy danh sách
       
        
        $thongtin=$this->Lien_He_Model->get_Thong_Tin($id);
        if(!$thongtin)
        {
            
            $this->session->set_flashdata('thongbao','Sản Phẩm Này Không Tồn Tại!.');
            redirect(base_url('admin/Lien_He/index'));
            
        }
        $data['thongtin']=$thongtin;
        
        if($this->input->post())
        {
            $this->form_validation->set_rules('phone','Số Điện Thoại','required');
            $this->form_validation->set_rules('address','địa chỉ','required');
            $this->form_validation->set_rules('email','email','required');
            
            if($this->form_validation->run())
            {
                
                $phone=$this->input->post("phone");
                $address=$this->input->post("address");
                $email=$this->input->post("email");
                $map=$this->input->post("map");
                
                $data=array(
                        'phone'=>$phone,
                        'address'=>$address,
                        'email'=>$email,
                        'map'=> $map
                        
                        
                        
                );
                
                if($this->Lien_He_Model->sua($id,$data))
                {
                    $this->session->set_flashdata('thongbao','Bạn Đã Cập Nhật Thành Công!.');
                }
                else {
                    $this->session->set_flashdata('thongbao','Bạn Cập Nhật Không Thành Công,Vui Lòng Kiểm Tra Lại.');
                }
                redirect(base_url('admin/Lien_He/index'));
                
            }
            
            
           
                
        }
        
        
        
        
        $data['temp']='admin/lienhe/sua';
        $this->load->view('admin/main',$data);
    }
    function xoa()
    {
        $id=$this->uri->segment(4);
        $id=intval($id);
        
        
        $xoa=$this->Lien_He_Model->Xoa($id);
        if($xoa)
        {
            
            $this->session->set_flashdata('thongbao','Đã Xóa Thành Công');
            redirect(base_url('admin/Lien_He/index'));
            
        }
        
        $data['temp']='admin/lienhe/xoa';
        $this->load->view('admin/main',$data);
    }
}