<?php
class login extends MY_controller
{
    
    function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('Admin_model');
    }
    
    

function kiem_tra_DN()
{
    $taikhoan = $this->input->post('taikhoan');
    $matkhau = md5($this->input->post('matkhau'));
    
    $dieu_kien = array('email'=>$taikhoan,'password'=>$matkhau);
    if($this->Admin_model->Kiem_Tra($dieu_kien))
    {
        return true;
    }
    else
    {
        // tao 1 message thong bao dang nhap ko thanh cong
        $this->form_validation->set_message(__FUNCTION__,'Tài Khoản Hoặc Mật Khẩu Không Chính Xác');
        return false;
    }
}

function index()
{
    $data = array();
    if($this->input->post())
    {
        //goi den ham kiem tra dang nhap check_login
        $this->form_validation->set_rules('login','login','callback_kiem_tra_DN');
        if($this->form_validation->run())
        {
           $user=$this->input->post('taikhoan');
            $this->session->set_userdata('login',$user);
            redirect(base_url('admin/order'));
        }
    }
    $this->load->view('admin/Login/index');
}



}