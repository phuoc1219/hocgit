<?php
class logo extends My_controller
{
    function __construct(){
        parent::__construct();
       
        $this->load->helper('form');
        $this->load->model('logo_Model');
    }
    
    function index()
    {
       
        
        //         lấy tổng số danh sách
        $TongSo= $this->logo_Model->get_tongso();
        $data['tongso']=$TongSo;
        
        //load thư viện phân trang
        $this->load->library('pagination');
        
        $config=array();
        
        $config['total_rows']=  $TongSo; //tổng tất cả sản phẩm trong web
        $config['base_url']=base_url('admin/logo/index'); //Đường dẫn của từng đoạn phân trang ( link của các nút phân trang )
        $config['per_page']    = 10;//Số lượng phần tử hiển thị trên một trang.
        $config['uri_segment'] = 4;//Xác định phân đoạn chứa số trang
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);
        
        
        $config['full_tag_open'] = "<ul class='pagination '>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#" >';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        
        
        
        $config['prev_link'] = '<i class="fa fa-angle-double-left"></i>Trước';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        
        
        $config['next_link'] = 'Sau<i class="fa fa-angle-double-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
       
        //khỡi tạo phân trang
        $this->pagination->initialize($config);
        $input=array();
        
        $star=intval($this->uri->segment(4));
       
        $input['limit']=array( $config['per_page'],$star );
        
        
        
        
        //         lấy danh sách
        
        $DanhSach=$this->logo_Model->get_Danh_Sach($input);
        
        $data['list']=$DanhSach;
        
        
      
      
        
        
      
       
        
        //lấy ra nội dung thông báo ỏ them()
        $thongbao=$this->session->flashdata('thongbao');
        $data['thongbao']=$thongbao;
        
        
      
        
    
        $data['temp']='admin/logo/index';
        $this->load->view('admin/main',$data);
    }
    
    
    function them()
    {
        
       //upload sản phẩm
        
       if($this->input->post())
       {
          
           
        
          
               $this->load->library('libraries_upload');
               $upload_path='./upload/logo';
               $upload_data=$this->libraries_upload->upload($upload_path,'img_link');
               $img_link='';
              if(isset($upload_data['file_name']))
              {
                  $img_link=$upload_data['file_name'];
              }
              
             
               $data=array(
                       
                       'image_link'=>$img_link,
                      

               );
               
               if($this->logo_Model->them($data))
               {
                   $this->session->set_flashdata('thongbao','Bạn Đã Thêm Thành Công!.');
               }
               else {
                   $this->session->set_flashdata('thongbao','Bạn Thêm Không Thành Công,Vui Lòng Kiểm Tra Lại.');
               }
               redirect(base_url('admin/logo/index'));
               
           
           
       }

        $data['temp']='admin/logo/them';
        $this->load->view('admin/main',$data);
    }
    
    function sua()
    {
        
        
        //lấy id quản trị cần chỉnh sữa
        $id=$this->uri->segment(4);
        $id=intval($id);
        
        $logo=$this->logo_Model->get_Thong_Tin($id);
        if(!$logo)
        {
            
            $this->session->set_flashdata('thongbao','Sản Phẩm Này Không Tồn Tại!.');
            redirect(base_url('admin/logo/index'));
            
        }
        
        $data['logo']=$logo;
       
       
        
        //upload sản phẩm
        
        if($this->input->post())
        {
           
            
            
            
            //nhập liệu
           
                $this->load->library('libraries_upload');
                $upload_path='./upload/logo';
                $upload_data=$this->libraries_upload->upload($upload_path,'img_link');
                $img_link='';
                if(isset($upload_data['file_name']))
                {
                    $img_link=$upload_data['file_name'];
                }
               
                
                $data=array(
                    
                  
                    'id'=>$this->input->post('id'),

                        
                        
                        
                        
                        
                );
                if($img_link != '')
                {
                    $data['image_link'] = $img_link;
                }
                
                
                if($this->logo_Model->sua($id,$data))
                {
                    $this->session->set_flashdata('thongbao','Bạn Đã Cập Nhật Thành Công!.');
                }
                else {
                    $this->session->set_flashdata('thongbao','Bạn Cập Nhật Không Thành Công,Vui Lòng Kiểm Tra Lại.');
                }
                redirect(base_url('admin/logo/index'));
                
            
            
        }
        
     
    
        
        
        $data['temp']='admin/logo/sua';
        $this->load->view('admin/main',$data);
    }
    
    function xoa()
    {
        $id=$this->uri->segment(4);
        $id=intval($id);
        
        
        $xoa=$this->logo_Model->Xoa($id);
        if($xoa)
        {
            
            $this->session->set_flashdata('thongbao','Đã Xóa Thành Công');
            redirect(base_url('admin/logo/index'));
            
        }
        
        $data['temp']='admin/logo/xoa';
        $this->load->view('admin/main',$data);
    }
    
}

