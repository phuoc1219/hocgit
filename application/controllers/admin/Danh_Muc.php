<?php
class Danh_Muc extends My_controller
{
    function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('Danh_Muc_Model');
    }
    
    function index()
    {
        $input=array();
        //         lấy danh sách
        $DanhSach=$this->Danh_Muc_Model->get_Danh_Sach($input);
       
        $data['list']=$DanhSach;
        //         lấy tổng số danh sách
        $TongSo= $this->Danh_Muc_Model->get_tongso();
        $data['tongso']=$TongSo;
        
        
        //lấy ra nội dung thông báo ỏ them()
        $thongbao=$this->session->flashdata('thongbao');
        $data['thongbao']=$thongbao;
        
        
       
        $data['temp']='admin/danhmuc/index';
        $this->load->view('admin/main',$data);
    }
    
    function them()
    {
        
        
        if($this->input->post())
        {
            $this->form_validation->set_rules('name','Họ Tên','required');
           
           
            
            
            if($this->form_validation->run())
            {
                
                $name=$this->input->post("name");
                $parent_id=$this->input->post("parent_id");
                $sort_order=$this->input->post("sort_order");
               
                $data=array(
                        'name'=>$name,
                        'parent_id'=>$parent_id,
                        'sort_order'=>$sort_order,
                        
                        
                        
                );
                
                if($this->Danh_Muc_Model->them($data))
                {
                    $this->session->set_flashdata('thongbao','Bạn Đã Thêm Thành Công!.');
                }
                else {
                    $this->session->set_flashdata('thongbao','Bạn Thêm Không Thành Công,Vui Lòng Kiểm Tra Lại.');
                }
                redirect(base_url('admin/Danh_Muc/index'));
                
            }
            
        }
        //lấy ra danh mục cha
        $input=array();
        $input['where']=array('parent_id'=>0);
        $DanhSach=$this->Danh_Muc_Model->get_Danh_Sach($input);
        $data['list']=$DanhSach;
        
        $data['temp']='admin/danhmuc/them';
        $this->load->view('admin/main',$data);
    }
    
    function sua()
    {
        
        //lấy id quản trị cần chỉnh sữa
        $id=$this->uri->segment(4);
        $id=intval($id);
        //lấy thông tin 
        
        $input=array();
        //         lấy danh sách
        $DanhSach=$this->Danh_Muc_Model->get_Danh_Sach($input);
        
        $data['list']=$DanhSach;
        
        
        $thongtin=$this->Danh_Muc_Model->get_Thong_Tin($id);
        if(!$thongtin)
        {
            
            $this->session->set_flashdata('thongbao','Sản Phẩm Này Không Tồn Tại!.');
            redirect(base_url('admin/Danh_Muc/index'));
            
        }
        if($this->input->post())
        {
           
            $this->form_validation->set_rules('name','Họ Tên','required');
            
            if($this->form_validation->run())
            {
                $name=$this->input->post("name");
                $parent_id=$this->input->post("parent_id");
                $sort_order=$this->input->post("sort_order");
                
                $data=array(
                        'name'=>$name,
                        'parent_id'=>$parent_id,
                        'sort_order'=>$sort_order,
                        
                        
                        
                );
                if($this->Danh_Muc_Model->Sua($id,$data))
                {
                    $this->session->set_flashdata('thongbao','Bạn Đã Cập Nhật Thành Công!.');
                }
                else {
                    $this->session->set_flashdata('thongbao','Bạn Cập Nhật Không Thành Công,Vui Lòng Kiểm Tra Lại.');
                }
                redirect(base_url('admin/Danh_Muc/index'));
            }
        }
        
        
        $data['thongtin']=$thongtin;
        $data['temp']='admin/danhmuc/sua';
        $this->load->view('admin/main',$data);
    }
    
    function xoa()
    {
        $id=$this->uri->segment(4);
        $id=intval($id);
        
        
        $xoa=$this->Danh_Muc_Model->Xoa($id);
        if($xoa)
        {
            
            $this->session->set_flashdata('thongbao','Đã Xóa Thành Công');
            redirect(base_url('admin/Danh_Muc/index'));
            
        }
        
        $data['temp']='admin/danhmuc/xoa';
        $this->load->view('admin/main',$data);
    }
}