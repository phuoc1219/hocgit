<?php
class Admin extends My_controller
{
    function __construct(){
       parent::__construct();
       $this->load->library('form_validation');
       $this->load->helper('form');
       $this->load->model('Admin_model');
    }
    
    function index()
    {
//         lấy danh sách
        $input=array();
        $DanhSach=$this->Admin_model->get_Danh_Sach($input);
        $data['list']=$DanhSach;
//         lấy tổng số danh sách
        $TongSo= $this->Admin_model->get_tongso();
        $data['tongso']=$TongSo;
        
       
        //lấy ra nội dung thông báo ỏ them()
        $thongbao=$this->session->flashdata('thongbao');
        $data['thongbao']=$thongbao;
        
        
        
        $data['temp']='admin/admin/index';
        $this->load->view('admin/main',$data);
        
    }
    
    //dùng để kiểm tra xem email có bị trùng hay không
    function Kiem_tra_email(){
        $email=$this->input->post("email");
        $kt=array('email'=>$email);
        if($this->Admin_model->kiem_tra($kt))
        {   
            //trả về thông báo lối
            $this->form_validation->set_message(__FUNCTION__,'email đã tồn tại,vui lòng nhập lại.');
            return FALSE;
        }
        else 
        {
            return true;
        }
    }
    
    
    function them(){
        
        
        $this->load->library('form_validation');
        $this->load->helper('form');
        if($this->input->post())
        {
           $this->form_validation->set_rules('name','Họ Tên','required|min_length[8]');
           $this->form_validation->set_rules('email','email','required|callback_Kiem_tra_email');
           $this->form_validation->set_rules('phone','số điện thoại','required');
           $this->form_validation->set_rules('address','địa chỉ','required');
           $this->form_validation->set_rules('password','mật Khẩu','required|min_length[8]');
           $this->form_validation->set_rules('text_password','Nhập Lại mật Khẩu','matches[password]');
           
           
           if($this->form_validation->run())
           {
               
               $name=$this->input->post("name");
               $email=$this->input->post("email");
               $phone=$this->input->post("phone");
               $address=$this->input->post("address");
               $password=$this->input->post("password");
               
               $data=array(
                       'name'=>$name, 
                       'email'=>$email,
                       'phone'=>$phone,
                       'address'=>$address,
                       'password'=>md5($password),
                       
                       
               );
                       
               if($this->Admin_model->them($data))
               {
                   $this->session->set_flashdata('thongbao','Bạn Đã Thêm Thành Công!.');
               }
               else {
                   $this->session->set_flashdata('thongbao','Bạn Thêm Không Thành Công,Vui Lòng Kiểm Tra Lại.');
               }
               redirect(base_url('admin/admin/index'));
               
           }
           
        }
      
        $data['temp']='admin/admin/them';
        $this->load->view('admin/main',$data);
        
    }
    
    //hàm chỉnh sữa thông tin 
    function Sua()
    {
        //lấy id quản trị cần chỉnh sữa
        $id=$this->uri->segment(4);
        $id=intval($id);
        //lấy thông tin quản trị viên
        $thongtin=$this->Admin_model->get_Thong_Tin($id);
        if(!$thongtin)
        {
            
            $this->session->set_flashdata('thongbao','Thành Viên Này Không Tồn Tại!.');
            redirect(base_url('admin/admin/index'));
           
        }
        
        
        
        if($this->input->post())
        {
            $this->form_validation->set_rules('name','Họ Tên','required|min_length[8]');
            $this->form_validation->set_rules('email','email','required|callback_Kiem_tra_email');
            $this->form_validation->set_rules('phone','số điện thoại','required');
            $this->form_validation->set_rules('address','địa chỉ','required');
           
          
            if($this->input->post("password"))
            {
                $this->form_validation->set_rules('password','mật Khẩu','required|min_length[8]');
                $this->form_validation->set_rules('text_password','Nhập Lại mật Khẩu','matches[password]');
            }
            if($this->form_validation->run())
            {
                
                $name=$this->input->post("name");
                $email=$this->input->post("email");
                $phone=$this->input->post("phone");
                $address=$this->input->post("address");
                $password=$this->input->post("password");
                
              
                
              
                $data=array(
                        'name'=>$name, 
                        'email'=>$email,
                        'phone'=>$phone,
                        'address'=>$address,
                        
                       
                       
                      
                        
                );
                if($password)
                {
                    $data['password']= md5($password);
                    
                }
                
                if($this->Admin_model->Sua($id,$data))
                {
                    $this->session->set_flashdata('thongbao','Bạn Đã Cập Nhật Thành Công!.');
                }
                else {
                    $this->session->set_flashdata('thongbao','Bạn Cập Nhật Không Thành Công,Vui Lòng Kiểm Tra Lại.');
                }
                redirect(base_url('admin/admin/index'));
                
            }
            
        }
        
        
        
        $data['thongtin']=$thongtin;
        
        $data['temp']='admin/admin/Sua';
        $this->load->view('admin/main',$data);
        
    }
    
    function Xoa()
    {
        $id=$this->uri->segment(4);
        $id=intval($id);
        

        $xoa=$this->Admin_model->Xoa($id);
        if($xoa)
        {
            
            $this->session->set_flashdata('thongbao','Đã Xóa Thành Công');
            redirect(base_url('admin/admin/index'));
            
        }
        
        $data['temp']='admin/admin/Xoa';
        $this->load->view('admin/main',$data);
    }
    
   
}