<?php
class San_Pham extends My_controller
{
    function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('San_Pham_Model');
    }
    
    function index()
    {
       
        
        //         lấy tổng số danh sách
        $TongSo= $this->San_Pham_Model->get_tongso();
        $data['tongso']=$TongSo;
        
        //load thư viện phân trang
        $this->load->library('pagination');
        
        $config=array();
        
        $config['total_rows']=  $TongSo; //tổng tất cả sản phẩm trong web
        $config['base_url']=base_url('admin/San_Pham/index'); //Đường dẫn của từng đoạn phân trang ( link của các nút phân trang )
        $config['per_page']    = 5;//Số lượng phần tử hiển thị trên một trang.
        $config['uri_segment'] = 4;//Xác định phân đoạn chứa số trang
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);
        
        
        $config['full_tag_open'] = "<ul class='pagination '>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#" >';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        
        
        
        $config['prev_link'] = '<i class="fa fa-angle-double-left"></i>Trước';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        
        
        $config['next_link'] = 'Sau<i class="fa fa-angle-double-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
       
        //khỡi tạo phân trang
        $this->pagination->initialize($config);
        $input=array();
        
        $star=intval($this->uri->segment(4));
       
        $input['limit']=array( $config['per_page'],$star );
        
        
        
        
        //         lấy danh sách
        
        $DanhSach=$this->San_Pham_Model->get_Danh_Sach($input);
        
        $data['list']=$DanhSach;
        
        
      
      
        
        
      
       
        
        //lấy ra nội dung thông báo ỏ them()
        $thongbao=$this->session->flashdata('thongbao');
        $data['thongbao']=$thongbao;
        
        
      
        
    
        $data['temp']='admin/sanpham/index';
        $this->load->view('admin/main',$data);
    }
    
    function chitiet()
    {
        //lấy id quản trị cần chỉnh sữa
        $id=$this->uri->segment(4);
        $id=intval($id);
        //lấy thông tin quản trị viên
        $thongtin=$this->San_Pham_Model->get_Thong_Tin($id);
        if(!$thongtin)
        {
            
            $this->session->set_flashdata('thongbao','Thành Viên Này Không Tồn Tại!.');
            redirect(base_url('admin/San_Pham/index'));
            
        }
        $data['chitiet']=$thongtin;
       
        $data['temp']='admin/sanpham/chitiet';
        $this->load->view('admin/main',$data);
    }
    
    
    function them()
    {
        
        //lấy danh sách danh mục sản phẩm
        //lay danh sach danh muc san pham
        $this->load->model('Danh_Muc_Model');
        $input = array();
        $input['where'] = array('parent_id' => 0);
        $danhmuc = $this->Danh_Muc_Model->get_Danh_Sach($input);
        foreach ($danhmuc as $row)
        {
            $input['where'] = array('parent_id' => $row->id);
            $subs = $this->Danh_Muc_Model->get_Danh_Sach($input);
            $row->subs = $subs;
        }
       
        $data['danhmuc'] = $danhmuc;
        
        //upload sản phẩm
        
        if($this->input->post())
        {
            $this->form_validation->set_rules('name','Họ Tên','required');
            $this->form_validation->set_rules('catalog','danh mục sản phẩm','required');
            $this->form_validation->set_rules('price','giá sản phẩm','required');
            
            
            
            //nhập liệu
            if($this->form_validation->run())
            {
                $this->load->library('libraries_upload');
                $upload_path='./upload/san_pham';
                $upload_data=$this->libraries_upload->upload($upload_path,'img_link');
                $img_link='';
               if(isset($upload_data['file_name']))
               {
                   $img_link=$upload_data['file_name'];
               }
               //upload cac anh kem theo
              
                
                $data=array(
                        'name'=>$this->input->post('name'),
                        'category_id'=>$this->input->post('catalog'),
                        'price'=>$this->input->post('price'),
                        'image_link'=>$img_link,
                        
                        'discount'=>$this->input->post('discount'),
                        'content'=>$this->input->post('content'),
                        'created'=>$this->input->post('ngaytao'),
                        'total'=>$this->input->post('total'),
                       
                        
                        
                        
                        
                        
                );
                
                if($this->San_Pham_Model->them($data))
                {
                    $this->session->set_flashdata('thongbao','Bạn Đã Thêm Thành Công!.');
                }
                else {
                    $this->session->set_flashdata('thongbao','Bạn Thêm Không Thành Công,Vui Lòng Kiểm Tra Lại.');
                }
                redirect(base_url('admin/San_Pham/index'));
                
            }
            
        }
       
        
        
      
        
        
      
        
        $data['temp']='admin/sanpham/them';
        $this->load->view('admin/main',$data);
    }
    
    function sua()
    {
        
        
        //lấy id quản trị cần chỉnh sữa
        $id=$this->uri->segment(4);
        $id=intval($id);
        
        $product=$this->San_Pham_Model->get_Thong_Tin($id);
        if(!$product)
        {
            
            $this->session->set_flashdata('thongbao','Sản Phẩm Này Không Tồn Tại!.');
            redirect(base_url('admin/Danh_Muc/index'));
            
        }
        
        $data['product']=$product;
      
        
        
        //lay danh sach danh muc san pham
        $this->load->model('Danh_Muc_Model');
        $input = array();
        $input['where'] = array('parent_id' => 0);
        $danhmuc = $this->Danh_Muc_Model->get_Danh_Sach($input);
        foreach ($danhmuc as $row)
        {
            $input['where'] = array('parent_id' => $row->id);
            $subs = $this->Danh_Muc_Model->get_Danh_Sach($input);
            $row->subs = $subs;
        }
        
        $data['danhmuc'] = $danhmuc;
        
        //upload sản phẩm
        
        if($this->input->post())
        {
            $this->form_validation->set_rules('name','Họ Tên','required');
            $this->form_validation->set_rules('catalog','danh mục sản phẩm','required');
            $this->form_validation->set_rules('price','giá sản phẩm','required');
            
            
            
            //nhập liệu
            if($this->form_validation->run())
            {
                $this->load->library('libraries_upload');
                $upload_path='./upload/san_pham';
                $upload_data=$this->libraries_upload->upload($upload_path,'img_link');
                $img_link='';
                if(isset($upload_data['file_name']))
                {
                    $img_link=$upload_data['file_name'];
                }
               
                
                $data=array(
                        'name'=>$this->input->post('name'),
                        'category_id'=>$this->input->post('catalog'),
                        'price'=>$this->input->post('price'),
                        'created'=>$this->input->post('ngaytao'),
                        'discount'=>$this->input->post('discount'),
                        'content'=>$this->input->post('content'),
                        'total'=>$this->input->post('total'),
                        
                        
                        
                        
                        
                        
                );
                if($img_link != '')
                {
                    $data['image_link'] = $img_link;
                }
                
                
                if($this->San_Pham_Model->sua($id,$data))
                {
                    $this->session->set_flashdata('thongbao','Bạn Đã Cập Nhật Thành Công!.');
                }
                else {
                    $this->session->set_flashdata('thongbao','Bạn Cập Nhật Không Thành Công,Vui Lòng Kiểm Tra Lại.');
                }
                redirect(base_url('admin/San_Pham/index'));
                
            }
            
        }
        
     
    
        
        
        $data['temp']='admin/sanpham/sua';
        $this->load->view('admin/main',$data);
    }
    
    function xoa()
    {
        $id=$this->uri->segment(4);
        $id=intval($id);
        
        
        $xoa=$this->San_Pham_Model->Xoa($id);
        if($xoa)
        {
            
            $this->session->set_flashdata('thongbao','Đã Xóa Thành Công');
            redirect(base_url('admin/San_Pham/index'));
            
        }
        
        $data['temp']='admin/sanpham/xoa';
        $this->load->view('admin/main',$data);
    }
    
}

