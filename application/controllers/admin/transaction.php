<?php
class transaction extends My_controller
{
    function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('transaction_model');
    }
    
    function index()
    {
       
        
        //         lấy tổng số danh sách
        $TongSo= $this->transaction_model->get_tongso();
        $data['tongso']=$TongSo;
        
        //load thư viện phân trang
        $this->load->library('pagination');
        
        $config=array();
        
        $config['total_rows']=  $TongSo; //tổng tất cả sản phẩm trong web
        $config['base_url']=base_url('admin/transaction/index'); //Đường dẫn của từng đoạn phân trang ( link của các nút phân trang )
        $config['per_page']    = 10;//Số lượng phần tử hiển thị trên một trang.
        $config['uri_segment'] = 4;//Xác định phân đoạn chứa số trang
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);
        
        
        $config['full_tag_open'] = "<ul class='pagination '>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#" >';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        
        
        
        $config['prev_link'] = '<i class="fa fa-angle-double-left"></i>Trước';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        
        
        $config['next_link'] = 'Sau<i class="fa fa-angle-double-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
       
        //khỡi tạo phân trang
        $this->pagination->initialize($config);
        $input=array();
        
        $star=intval($this->uri->segment(4));
       
        $input['limit']=array( $config['per_page'],$star );
        
        
        
        
        //         lấy danh sách
        
        $DanhSach=$this->transaction_model->get_Danh_Sach($input);
        
        $data['list']=$DanhSach;

        
        
      
      
        
        
      
       
     
        
    
        $data['temp']='admin/giao_dich/index';
        $this->load->view('admin/main',$data);
    }
}