<?php


class Home extends MY_Controller
{

	 function index()
	{
	   
	    
	    //sản phẩm mới
	    $this->load->model('San_Pham_Model');
		$input=array();
		
	    $input['limit']=array(15,0);
	    $sanpham=$this->San_Pham_Model->get_Danh_Sach($input);
	    
		$data['list']=$sanpham;
		
		//sản phẩm khuyến mãi
		$input['where']=array('discount >='=>'1');
		$input['limit']=array(12,0);
	    $khuyenmai=$this->San_Pham_Model->get_Danh_Sach($input);
		$data['KM']=$khuyenmai;
		
		
	    //gọi thư viện cart
		 $this->load->library('cart');
		 $data['tongso_sp']=$this->cart->total_items();
		 
		







		
		
	    //lấy ra nội dung thông báo
	    $thongbao=$this->session->flashdata('thongbao');
	    $data['thongbao']=$thongbao;
		
		//lấy ra danh mục sản phẩm
		$this->load->model('Danh_Muc_Model');
		$input = array();
		$input['where'] = array('parent_id' => 0);
		$catelog = $this->Danh_Muc_Model->get_Danh_Sach($input);
		foreach ($catelog as $row)
		{
			$input['where'] = array('parent_id' => $row->id);
			$subs = $this->Danh_Muc_Model->get_Danh_Sach($input);
			$row->subs = $subs;
		}
		
		$data['catelog_sp'] = $catelog;
		
		//lấy danh sách slide
		$this->load->model('slide_Model');
		$DanhSach=$this->slide_Model->get_Danh_Sach();
        
        $data['list_hinh']=$DanhSach;

	
		$this->load->view('slide/home/index',$data);
		
	}
	
	
}
