<?php


class user extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('user_model');
    }
    
    function Kiem_tra_email(){
        $email=$this->input->post("email");
        $kt=array('email'=>$email);
        if($this->user_model->kiem_tra($kt))
        {
            //trả về thông báo lối
            $this->form_validation->set_message(__FUNCTION__,'email đã tồn tại,vui lòng nhập lại.');
            return FALSE;
        }
        else
        {
            return true;
        }
    }
    
    //dăng kí thành viên
	 function DangKy()
	{
	   
	    if($this->input->post())
	    {
	        $this->form_validation->set_rules('name','Họ Tên','required|min_length[8]');
	        $this->form_validation->set_rules('email','email','required|callback_Kiem_tra_email');
	        $this->form_validation->set_rules('phone','số điện thoại','required');
	        $this->form_validation->set_rules('address','địa chỉ','required');
	       
	        $this->form_validation->set_rules('password','mật Khẩu','required|min_length[8]');
	        $this->form_validation->set_rules('text_password','Nhập Lại mật Khẩu','matches[password]');
	        
	        
	        if($this->form_validation->run())
	        {
	            
	            $name=$this->input->post("name");
	            $email=$this->input->post("email");
	            $phone=$this->input->post("phone");
	            $address=$this->input->post("address");
	          
	            $password=$this->input->post("password");
	            
	            $data=array(
	                    'name'=>$name,
	                    'email'=>$email,
	                    'phone'=>$phone,
	                    'address'=> $address,
	                    'password'=>md5($password),
	                    
	                    
	            );
	            
	            if($this->user_model->them($data))
	            {
	                $this->session->set_flashdata('thongbao','Bạn Đã Đăng Ký Thành Công!.');
                    
	            }
	            else {
	                $this->session->set_flashdata('thongbao','Bạn Đăng Ký Không Thành Công,Vui Lòng Kiểm Tra Lại.');
	            }
	          
	            redirect(base_url('user/dangky'));
	            
	        }
	        
	    }
	    //lấy ra nội dung thông báo 
	    $thongbao=$this->session->flashdata('thongbao');
	    $data['thongbao']=$thongbao;
	    
	    
	    
		$data['temp']='slide/user/dangky';
		$this->load->view('slide/layout', $data);
		
	}
	
	function dangnhap()
	{
	    
	    
	    if($this->input->post())
	    {
	        $this->form_validation->set_rules('email', 'Email đăng nhập', 'required');
	        $this->form_validation->set_rules('pass', 'Mật khẩu', 'required|min_length[6]');
	        $this->form_validation->set_rules('login' ,'login', 'callback_check_login');
	        if($this->form_validation->run())
	        {
	            //lay thong tin thanh vien
	            $user = $this->_get_user_info();
	            //gắn session id của thành viên đã đăng nhập
	            $this->session->set_userdata('user_id_login', $user->id);
	            
	            $this->session->set_flashdata('thongbao', 'Đăng nhập thành công');
	            redirect();
	        }
	    }
	    
	    
	   
	    $this->load->view('slide/user/dangnhap');
	}
	function check_login()
	{
	    $user = $this->_get_user_info();
	    if($user)
	    {
	        return true;
	    }
	    $this->form_validation->set_message(__FUNCTION__, 'Không đăng nhập thành công');
	    return false;
	}
	
	
	//lấy thông tin người dùng
	private  function _get_user_info()
	{
	    $email = $this->input->post('email');
	    $password = $this->input->post('pass');
	    $password = md5($password);
	    
	    $where = array('email' => $email , 'password' => $password);
	    $user = $this->user_model->get_Thong_Tin_DK($where);
	    return $user;
	}
	function logout()
	{
	    if($this->session->userdata('user_id_login'))
	    {
	        $this->session->unset_userdata('user_id_login');
	    }
	    $this->session->set_flashdata('message', 'Đăng xuất thành công');
	    redirect();
	}
	function index()
	{
	    if(!$this->session->userdata('user_id_login'))
	    {
	        redirect();
	    }
	    $user_id = $this->session->userdata('user_id_login');
	    $user = $this->user_model->get_Thong_Tin($user_id);
	    if(!$user)
	    {
	        redirect();
	    }
	    
	    $data['thongtin_user']=$user;
	    $data['temp']='slide/user/thongtin_user';
	    $this->load->view('slide/layout', $data);
	    
	    
	}
	function sua()
	{
	    
	   

	    if(!$this->session->userdata('user_id_login'))
	    {
	        redirect();
	    }
	    $user_id = $this->session->userdata('user_id_login');
	    $user = $this->user_model->get_Thong_Tin($user_id);
	    if(!$user)
	    {
	        redirect();
	    }
	    
	    $data['thongtin_sua']=$user;
	    
	    
	    
	    
	    
	    if($this->input->post())
	    {
	        
	        $password=$this->input->post("password");
	        if($password)
	        {
	            $this->form_validation->set_rules('password','mật Khẩu','min_length[8]');
	            $this->form_validation->set_rules('text_password','Nhập Lại mật Khẩu','matches[password]');
	            
	        }
	        
	        $this->form_validation->set_rules('name','Họ Tên','required|min_length[8]');
	        
	        $this->form_validation->set_rules('phone','số điện thoại','required');
	        $this->form_validation->set_rules('address','địa chỉ','required');
	        
	        
	        
	        if($this->form_validation->run())
	        {
	            
	            $name=$this->input->post("name");
	            $phone=$this->input->post("phone");
	            $address=$this->input->post("address");
	            
	            
	            
	            $data=array(
	                    'name'=>$name,
	                    
	                    'phone'=>$phone,
	                    'address'=> $address,
	                    
	                    
	                    
	            );
	            
	            if($password)
	            {
	                $data['password']=md5($password);
	            }
	            if($this->user_model->sua($user_id,$data))
	            {
	                $this->session->set_flashdata('thongbao','Thông Tin Đã Cập Nhật Thành Công!.');
	                
	            }
	            else {
	                $this->session->set_flashdata('thongbao','Bạn Cập Nhật Không Thành Công,Vui Lòng Kiểm Tra Lại.');
	            }
	            
	            redirect(base_url('user/sua'));
	        }
	        
	        
	        
	    }
	    
	    
	    
	    //lấy ra nội dung thông báo
	    $thongbao=$this->session->flashdata('thongbao');
	    $data['thongbao']=$thongbao;
	    
	    $data['temp']='slide/user/sua';
	    $this->load->view('slide/layout', $data);
	}
}
